<?php

namespace App;

use App;
use Notification;
use Illuminate\Database\Eloquent\Model;
use App\Notifications\emails;

class Estagios extends Model
{
    protected $table = 'estagio';
    public $fillable = [
        'id', 
        'aluno_id', 
        'convenio_id', 
        'professor_id', 
        'carga_horaria', 
        'situacao',
        'termo_compromisso',
        'carta_apresentacao',
        'plano_estagio',
        'relatorio_estagiario',
        'relatorio_concedente',
        'avaliacao_estagiario',
        'controle_frequencia',
        'access',
    ];
    protected $hidden = ['access'];
    
    public function convenio() {
        return $this->belongsTo('App\Convenios');
    }
    
    public function professor() {
        return $this->belongsTo('App\Professores');
    }
    
    public function aluno() {
        return $this->belongsTo('App\Alunos');
    }
    
    public function cargaHoraria() {
        return $this->hasMany('App\CargaHoraria','estagio_id');
    }
    
    public function checkStatus() {
        $situacao = 'Aprovado';
        
        if(($this->carga_horaria > 0)) {
            $situacao = 'Aguardando Documentação';
            
            if(($this->termo_compromisso == 'true') && 
                ($this->plano_estagio == 'true') && 
                ($this->controle_frequencia == 'true') && 
                ($this->relatorio_estagiario == 'true') && 
                ($this->relatorio_concedente == 'true') && 
                ($this->avaliacao_estagiario == 'true')) {
                $situacao = 'Concluido';

            }
        }
        if($this->situacao != $situacao) {
            $this->update(['situacao' => $situacao]);
        }
        
        $this->aluno->checkStatus();
        if ($this->situacao == 'Concluido') {
            $this->certificaProfessor();
        }
    }

    public function certificaProfessor() {
        // Cria o anexo
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf.certificado_professor', ['estagio' => $this]);
        $file_path = '/app/certificados/professor/'.$this->professor->id.'_'.$this->id.'.pdf'; 

        // $file_path = '/myfile.pdf'; 
        $pdf->save(storage_path().$file_path);

        // Notifica o professor com o anexo
        Notification::send($this->professor, new emails($this, $file_path)); 
        return true; 
    }
}
