<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaAtuacao extends Model
{
    protected $table = 'area';
    public $fillable = ['id','nome','descricao','access',];
    protected $hidden = ['access'];
}
