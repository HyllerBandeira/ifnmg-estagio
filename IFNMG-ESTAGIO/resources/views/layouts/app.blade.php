<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{url('/img/logoFinal.png')}}" type="image/png" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>IFNMG - Estágio</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    <!-- BOOTSTRAP DATEPICKER -->
    <link href="{{ asset('css/components/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <!--/ BOOTSTRAP DATEPICKER -->

    <!-- SELECT2 COM BUSCA -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" /> 
    <!--/ SELECT2 COM BUSCA -->

    <!-- DATA TABLE -->
    <link href="{{ asset('css/components/data-table/datatables.min.css')}}" rel="stylesheet">
    <!--/ DATA TABLE -->
    
    <!-- ICONS -->
    <link href="https://cdn.materialdesignicons.com/3.2.89/css/materialdesignicons.min.css" rel="stylesheet">
    <!--/ ICONS -->

    <style>
	    .navbar-style {
		background: #ddd;
		color: #000;
	    }
	    .navbar-style li:hover {
   		color: #fff; // links
	   }
        /* Tooltip */
        .alerta + .tooltip > .tooltip-inner {
              background-color: #f2dede; 
              color: #a94442; 
              border: 1px solid red; 
              padding: 10px;
              font-size: 13px;
        }
        .alerta + .tooltip.top > .tooltip-arrow {
          border-top: 5px solid red;
        }
        
        
        .letra{
            font-size: 20px;
            color: red;
        }
        .letraV{
            font-size: 16px;
            color: green;
            font-stretch: condensed; 
        }
        .letra1{
            font-size: 40px;
            color: red;
        }
        .modalSuperior{
            z-index: 20000;
        }
        .modal-lg{
            width: 1100px !important;
        }
	</style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-static-top navbar-default">
<!--        <nav class="navbar navbar-default navbar-static-top navbar-style">-->
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-left" href="{{ url('/') }}">
                        <img src="{{URL::asset('img/logo/logo_almenara.jpg')}}" width="150x" style="padding: 2px 0px 5px 0px !important">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @auth
                            @can('user.admin')
                                <li class="nav_item"><a href="{{ route('documento.indexPublic') }}">Documentos</a></li>
                                <li class="nav_item"><a href="{{ route('curso.index') }}">Cursos</a></li>
                                <li class="nav_item"><a href="{{ route('professor.index') }}">Professores</a></li>
                                <li class="nav_item"><a href="{{ route('convenio.indexPublic') }}">Convênios</a></li>
                                <li class="nav_item"><a href="{{ route('aluno.index') }}">Alunos</a></li>
                                <li class="nav_item"><a href="{{ route('estagio.index') }}">Estágio</a></li>
                            @else
                                <li class="nav_item"><a href="{{ route('documento.indexPublic') }}">Documentos</a></li>
                                <li class="nav_item"><a href="{{ route('convenio.indexPublic') }}">Convênios</a></li>
                            @endcan
                                <li class="nav_item dropdown">
                                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                Sair
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                        @can('user.admin')
                                            <li><a href="{{ route('register') }}">Registrar</a></li>
                                        @endcan
                                        @can('user.verMeuEstagio')
                                            <li><a href="{{ route('aluno.meusEstagios') }}">Meus estágios</a></li>
                                        @endcan
                                    </ul>
                                </li>
                        @else
                            <li class="nav_item"><a href="{{ route('documento.indexPublic') }}">Documentos</a></li>
                            <li class="nav_item"><a href="{{ route('convenio.indexPublic') }}">Convênios</a></li>
                            <li class="nav_item"><a href="{{ route('login') }}">Login</a></li>
                        @endauth
                        
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                @yield('content')
            </div>
        </div>
<!--        MODAL CONFIRM SUBMIT -->
        <div class="modal fade modalSuperior" id="confirm-submit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content"></div>
            </div>
            <div class="modal-dialog">
                <div class="modal-content"></div>
            </div>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div id="modal-participar" class="modal-header bg-danger">
                        <h4 class="modal-title" id="myModalLabel">Confirmação</h4>
                        <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span></button>

                    </div>
                    <div class="modal-body">

                        Deseja excluir o registro?

                    </div>
                    <div class="modal-footer">
                        <button type="button" id="confirm-modal-btn-close" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="submit" id="confirm-modal-btn-submit" class="btn btn-danger">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        @yield('modal')
    </div>
     <!-- Tooltip -->
    <script src="https://unpkg.com/tooltip.js@1.3.0/dist/umd/tooltip.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!--/ Tooltip -->
    
    <!-- Scripts -->
<!--    <script src="{{ asset('js/app.js') }}"></script>-->
   
<!--   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>-->
    <!-- SELECT2 COM BUSCA -->
<!--     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <!--/ SELECT2 COM BUSCA -->

    <!-- BOOTSTRAP DATEPICKER -->
    <script src="{{ asset('js/components/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('js/components/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js')}}"></script>
    <!--/ BOOTSTRAP DATEPICKER -->

    <!-- JQUERY MASK -->
    <script src="{{ asset('js/components/jquery-mask/jquery.mask.js')}}"></script>
    <!--/ JQUERY MASK -->
    <!-- DATA TABLE -->
    <script src="{{ asset('js/components/data-table/datatables.min.js')}}"></script>
    <!--/ DATA TABLE -->

    @yield('script')
    <script>
        $(document).ready(function() {
            var form;
            
            setTimeout(function() {
                $('.flash-message').fadeOut('slow');
            }, 5000);
            
            if($('.table-ordering')) {
                $('.table-ordering').each(function() {
                    var config = {
                        "language": {
                            "url": "//cdn.datatables.net/plug-ins/1.10.18/i18n/Portuguese-Brasil.json"
                        },
                        scrollY: 500,
                        paging: true,
                    };
                    var scrollY = $(this).attr('table-max-height');
                    if(scrollY)
                        config.scrollY = scrollY;
                    
                    var paging = $(this).attr('table-paging');
                    if(paging == 'false')
                        config.paging = false;
                    $(this).DataTable(config);
                });
            }
            $('[data-toggle="tooltip"]').tooltip('show');
            
            
            //Controle para confirmar exclusão
            $('.form-delete-cofirm').on('submit', function(e) {
                var confirmed = $(this).data('confirmed');
                if (confirmed == false) {
                    e.preventDefault();
                    $('#confirm-submit-modal').modal();
                    form = $(this);
                }
            })
            
            //Controle para confirmar exclusão
            $('#confirm-modal-btn-submit').on('click', function(e) {
                form.data('confirmed', 'true');
                form.submit();
                form = null;
            })
            
            $('#confirm-submit-modal').on('hide.bs.modal', function(e) {
                form = null;
            })
        });
        
        // Função para restringir os valores dos campos que só devem aceitar numeros
        $('.numbersOnly').keyup(function() {
            this.value = this.value.replace(/[^0-9\,]/g, '');
        });
    </script>
</body>
</html>
