<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class emails extends Notification
{
    use Queueable;

    public $estagio;
    public $file_path;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($estagio, $file_path)
    {
        $this->estagio = $estagio;
        $this->file_path = $file_path;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Certificado de Orientação de Estágio')
            ->markdown('layouts.email')
            ->line('O Aluno '.$this->estagio->aluno->nome.' finalizou o estágio.')
            ->attach(
                storage_path("{$this->file_path}"),
                ['as' => 'Certificado '. $this->estagio->aluno->nome.'.pdf']
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
