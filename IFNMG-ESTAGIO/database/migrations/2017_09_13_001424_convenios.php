<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Convenios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convenio', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('tipo', ['fisica','juridica'])->nullable();
            $table->string('nome')->nullable();
            $table->string('nome_fantasia')->nullable();
            $table->string('ramo_atividade')->nullable();
            $table->string('cpf_cnpj')->nullable();
            $table->string('endereco')->nullable();
            $table->string('numero')->nullable();
            $table->string('complemento')->nullable();
            $table->string('cadastro_convenio')->nullable();
            $table->string('bairro')->nullable();
            $table->string('cidade')->nullable();
            $table->string('estado')->nullable();
            $table->string('cep')->nullable();
            $table->string('telefone')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('representante_legal')->nullable();
            $table->string('cargo_habilitacao')->nullable();
            $table->string('conselho_classe')->nullable();
            $table->string('numero_registro')->nullable();
            $table->string('access')->default('almenara')->notNull();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
