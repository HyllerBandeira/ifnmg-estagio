<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cursos extends Model
{
    protected $table = 'curso';
    public $fillable = ['id','nome','descricao','carga_horaria','access'];
    protected $hidden = ['access'];
}
