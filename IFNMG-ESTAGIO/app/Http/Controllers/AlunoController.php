<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Auth;
use App\Alunos;
use App\Cursos;
use App\Estagios;
use App\Pessoas;
use App\User;

class AlunoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $access = Auth::user()->access ?? env('APP_ACCESS');

        $alunos = Alunos::where('access', $access)->orderBy('id','DESC')->get();

        return view('aluno.index',compact('alunos'));
    }
    /**
     * Caso tenha mais de um registro de aluno para o usuário retorna uma lista para seleção. Caso não, informa o detalhamento do único estágio
     *
     * @param  int $aluno_id
     * @return \Illuminate\Http\Response
     */
    public function meusEstágios($aluno_id = null)
    {
        $aluno = Alunos::find($aluno_id);

        if($aluno) {
            $aluno->pessoa->nascimento = implode('/',array_reverse(explode('-',$aluno->pessoa->nascimento)));

            return view('aluno.meuEstagio', ['aluno'=>$aluno])->with('warning','Você possúi apenas um estágio cadastrado');
        };

        $alunos = Alunos::whereHas('pessoa', function($query) {
                $query->where([
                    ['nome', Auth::user()->name],
                    ['email', Auth::user()->email],
                    ['access', Auth::user()->access],
                ]);
            })->orderBy('id','DESC')->get();

        if($alunos->count() < 1)
            return redirect()->route('documento.indexPublic')
                ->with('error','Você não possúi estágio cadastrado');

        else if($alunos->count() == 1) {
            $aluno = $alunos->first();
            $aluno->pessoa->nascimento = implode('/',array_reverse(explode('-',$aluno->pessoa->nascimento)));

            return view('aluno.meuEstagio', ['aluno'=>$aluno])->with('warning','Você possúi apenas um estágio cadastrado');
        }
        return view('aluno.meusEstagios',compact('alunos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $retorno = Cursos::orderBy('id','DESC')->get();
        $cursos;
        foreach($retorno as $curso) {
            $cursos[$curso->id] = $curso->nome;
        }
        $error = 0;
        $errorMessage = [];
        if(empty($cursos)) {
            $cursos = [];
            $errorMessage[] = 'Não existem cursos cadastrados;';
            $error = true;
        }

        $cursos[0] = '';

        if($error)
            return view('aluno.create', ['cursos'=> $cursos])->withErrors($errorMessage);

        return view('aluno.create', ['cursos'=> $cursos]);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nome' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required'  => 'Campo Obrigratório.',
        ];
        // Validação dos campos necessários
        $this->validate($request, [
            'nome' => 'required',
            'turma' => 'required',
            'cidade_natal' => 'required',
            'uf_cidade_natal' => 'required',
            'nascimento' => 'required',
            'filiacao' => 'required',
            'estado_civil' => 'required',
            'sexo' => 'required',
            'rg' => 'required',
            'orgao_expeditor' => 'required',
            'cpf' => 'required',
            'email' => 'required',
            'endereco' => 'required',
            'numero' => 'required',
            'bairro' => 'required',
            'cidade' => 'required',
            'uf' => 'required',
            'cep' => 'required',
            'ficha_matricula' => 'required',
            'matricula' => 'required|unique:aluno',
        ], [
            'matricula.unique' => 'Esse Número da Matrícula já está sendo usado sistema, coloque outro número de matrícula',
            'cpf.required' => 'Campo Obrigratório',
            'nome.required' => 'Campo Obrigratório',
            'turma.required' => 'Campo Obrigratório',
            'cidade_natal.required' => 'Campo Obrigratório',
            'uf_cidade_natal.required' => 'Campo Obrigratório',
            'nascimento.required' => 'Campo Obrigratório',
            'filiacao.required' => 'Campo Obrigratório',
            'estado_civil.required' => 'Campo Obrigratório',
            'sexo.required' => 'Campo Obrigratório',
            'rg.required' => 'Campo Obrigratório',
            'orgao_expeditor.required' => 'Campo Obrigratório',
            'email.required' => 'Campo Obrigratório',
            'endereco.required' => 'Campo Obrigratório',
            'numero.required' => 'Campo Obrigratório',
            'bairro.required' => 'Campo Obrigratório',
            'cidade.required' => 'Campo Obrigratório',
            'uf.required' => 'Campo Obrigratório',
            'cep.required' => 'Campo Obrigratório',
            'ficha_matricula.required' => 'Campo Obrigratório',
            'matricula.required' => 'Campo Obrigratório',
        ]);

        // Formatação dos campos necessários
        $request['nascimento'] = implode('-',array_reverse(explode('/',$request->nascimento)));

        // Adição do controle de acesso
        $request['access'] = Auth::user()->access;

        // Salva os dados de pessoa na tabela pessoa, pegando o id para relacionamento
        $pessoa = Pessoas::create($request->all());

        $aluno = Alunos::create([
            'pessoa_id' => $pessoa->id,
            'curso_id' => $request->curso_id,
            'matricula' => $request->matricula,
            'turma' => $request->turma,
            'access' => Auth::user()->access,
        ]);

        // UPLOAD DOS ARQUIVOS
        if($request->file('ficha_matricula') != null) {
            $path_ficha_matricula = $request->file('ficha_matricula')
                ->storeAs("aluno/$aluno->id/pdfs","ficha_matricula.pdf");
            $aluno_data['ficha_matricula'] = 'true';

            $aluno->update($aluno_data);
        }

        if($request->file('foto') != null) {
            $path_ficha_matricula = $request->file('foto')
                ->storeAs("aluno/$aluno->id/fotos",$request->foto->getClientOriginalName());
            $aluno_foto['foto'] = $request->foto->getClientOriginalName();

            $aluno->update($aluno_foto);
        }
        
        $credencial = User::where([
            ['email', $pessoa->email],
            ['access', Auth::user()->access],
            ['auth', 'aluno'],
        ])->first();
        
        if (!$credencial) {
            $credencial = User::create([
                'name' => $pessoa->nome,
                'email' => $pessoa->email,
                'password' => bcrypt(preg_replace("/[^0-9]/","",$pessoa->cpf)),
                'access' => Auth::user()->access,
                'auth' => 'aluno',
            ]);
        }

        return redirect()->route('aluno.index')
            ->with('success','Registro salvo com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  Alunos  $aluno
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $aluno = Alunos::findOrFail($id);

        $aluno->pessoa->nascimento = implode('/',array_reverse(explode('-',$aluno->pessoa->nascimento)));

        $estagios = Estagios::where('aluno_id','=',$id)->orderBy('id','DESC')->get();

        return view('aluno.show',['estagios'=>$estagios, 'aluno'=>$aluno]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Alunos  $aluno
     * @return \Illuminate\Http\Response
     */
    public function edit(Alunos $aluno)
    {
        if (Gate::denies('aluno.controlar', $aluno)) {
            return abort(403, "Você não tem acesso a esta função");
        }

        $aluno->pessoa->nascimento = implode('/',array_reverse(explode('-',$aluno->pessoa->nascimento)));

        $retorno = Cursos::orderBy('id','DESC')->get();
        $cursos;
        foreach($retorno as $curso) {
            $cursos[$curso->id] = $curso->nome;
        }
        $error = 0;
        $errorMessage = [];
        if(empty($cursos)) {
            $cursos = [];
            $errorMessage[] = 'Não existem cursos cadastrados;';
            $error = true;
        }

        if($error)
            return view('aluno.edit',['aluno'=>$aluno, 'cursos'=>$cursos])->withErrors($errorMessage);

        return view('aluno.edit',['aluno'=>$aluno, 'cursos'=>$cursos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Alunos  $aluno
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Alunos $aluno)
    {
        if (Gate::denies('aluno.controlar', $aluno)) {
            return abort(403, "Você não tem acesso a esta função");
        }

        // Validação dos campos necessários
        $this->validate($request, [
            'nome' => 'required',
            'cidade_natal' => 'required',
            'turma' => 'required',
            'uf_cidade_natal' => 'required',
            'nascimento' => 'required',
            'filiacao' => 'required',
            'estado_civil' => 'required',
            'sexo' => 'required',
            'rg' => 'required',
            'orgao_expeditor' => 'required',
            'cpf' => 'required',
            'endereco' => 'required',
            'numero' => 'required',
            'bairro' => 'required',
            'cidade' => 'required',
            'uf' => 'required',
            'cep' => 'required',
        ], [
            'cpf.unique' => 'Esse CPF já foi cadastrado no sistema, coloque outro CPF',
            'matricula.unique' => 'Esse Número da Matrícula já está sendo usado sistema, coloque outro número de matrícula',
            'cpf.required' => 'Campo Obrigratório',
            'turma.required' => 'Campo Obrigratório',
            'nome.required' => 'Campo Obrigratório',
            'cidade_natal.required' => 'Campo Obrigratório',
            'uf_cidade_natal.required' => 'Campo Obrigratório',
            'nascimento.required' => 'Campo Obrigratório',
            'filiacao.required' => 'Campo Obrigratório',
            'estado_civil.required' => 'Campo Obrigratório',
            'sexo.required' => 'Campo Obrigratório',
            'rg.required' => 'Campo Obrigratório',
            'orgao_expeditor.required' => 'Campo Obrigratório',
            'email.required' => 'Campo Obrigratório',
            'endereco.required' => 'Campo Obrigratório',
            'numero.required' => 'Campo Obrigratório',
            'bairro.required' => 'Campo Obrigratório',
            'cidade.required' => 'Campo Obrigratório',
            'uf.required' => 'Campo Obrigratório',
            'cep.required' => 'Campo Obrigratório',
            'ficha_matricula.required' => 'Campo Obrigratório',
            'matricula.required' => 'Campo Obrigratório',
        ]);
        // Formatação dos campos necessários
        $request['nascimento'] = implode('-',array_reverse(explode('/',$request->nascimento)));

        // Seleciona o cadastro em pessoa
        $pessoa = Pessoas::findOrFail($aluno->pessoa_id);
        DB::connection()->enableQueryLog();
        // Pega a credencial em user
        $credencial = User::where([
            ['email', $pessoa->email],
            ['access', Auth::user()->access],
            ['auth', 'aluno'],
        ])->first();

        // Atualiza o cadastro em pessoa
        $pessoa->update($request->all());

        $aluno_data = [
            'pessoa_id' => $aluno->pessoa_id,
            'curso_id' => $request->curso_id,
            'matricula' => $request->matricula,
            'turma' => $request->turma,
        ];

        // UPLOAD DOS ARQUIVOS
        if($request->file('ficha_matricula')) {
            $path_ficha_matricula = $request->file('ficha_matricula')
                                            ->storeAs("aluno/$aluno->id/pdfs","ficha_matricula.pdf");
            $aluno_data['ficha_matricula'] = 'true';
        }

        $aluno->update($aluno_data);

        if($request->file('foto')) {
            $path_ficha_matricula = $request->file('foto')
                                            ->storeAs("aluno/$aluno->id/fotos",$request->foto->getClientOriginalName());
            $aluno_foto['foto'] = $request->foto->getClientOriginalName();

            $aluno->update($aluno_foto);
        }

        if($credencial) {
            $credencial->update([
                'name' => $pessoa->nome,
                'email' => $pessoa->email,
                'password' => bcrypt(preg_replace("/[^0-9]/","",$pessoa->cpf)),
                'access' => Auth::user()->access,
                'auth' => 'aluno',
            ]);
        }
        else {
            $credencial = User::create([
                'name' => $pessoa->nome,
                'email' => $pessoa->email,
                'password' => bcrypt(preg_replace("/[^0-9]/","",$pessoa->cpf)),
                'access' => Auth::user()->access,
                'auth' => 'aluno',
            ]);
        }

        return redirect()->route('aluno.index')
                        ->with('success','Registro alterado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Alunos  $aluno
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alunos $aluno)
    {
        if (Gate::denies('aluno.controlar', $aluno)) {
            return abort(403, "Você não tem acesso a esta função");
        }

        try{

            $aluno->delete();

        } catch (\PDOException $e) {

            return redirect()->route('aluno.index')->with('error', 'Não é possível excluir o aluno, pois ele possui um estágio cadastrado');
        }

        return redirect()->route('aluno.index')
                        ->with('success','Registro deletado com sucesso');
    }

    /**
     * Get the specified document from storage.
     *
     * @param  Alunos  $aluno
     * @return \Illuminate\Http\Response
     */
    public function showPdf($aluno_id=0, $documento=0)
    {
        if(($aluno_id < 1))
            return abort(404, "Pessoa não foi encontrada");

        $file_path = "aluno/$aluno_id/pdfs/$documento.pdf";

//        dd(storage_path("app\\$file_path"));

        if(Storage::disk('local')->exists("/$file_path"))
            return response()->file(storage_path("app/$file_path"));

        return  abort(404, "Arquivo não foi encontrado");
    }

    /**
     * Get the specified document from storage.
     *
     * @param  Alunos  $aluno
     * @return \Illuminate\Http\Response
     */
    public function showFoto($aluno_id=0, $foto=0)
    {
        if(($aluno_id < 1))
            return abort(404, "Pessoa não foi encontrada");

        $file_path = "aluno/$aluno_id/fotos/$foto";

//        dd(storage_path("app\\$file_path"));

        if(Storage::disk('local')->exists("/$file_path"))
            return response()->file(storage_path("app/$file_path"));

        return  abort(404, "Arquivo não foi encontrado");
    }

    /**
     * Remove the specified document from storage.
     *
     * @param  Alunos  $aluno
     * @return \Illuminate\Http\Response
     */
    public function destroyPdf(Request $request, Alunos $aluno)
    {
        if (Gate::denies('aluno.controlar', $aluno)) {
            return abort(403, "Você não tem acesso a esta função");
        }

        $this->validate($request, [
            'documento' => 'required',
        ]);

        $data = [$request->documento => 'false'];

        $aluno->update($data);

        $file_path = "aluno/$id/pdfs/$request->documento.pdf";
        Storage::disk('local')->delete("/$file_path");

        return redirect()->route('aluno.edit', $id)
                            ->with('success','Documento removido com sucesso')
                            ->withInput();
    }
}