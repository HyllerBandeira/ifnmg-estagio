<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>IFNMG-ESTAGIO</title>
        <link rel="icon" href="{{url('/img/logoFinal.png')}}" type="image/png" />

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.light_green-red.min.css">
        
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style>
			.mdl-layout__header {
			background: #000;
			color: #fff;
			}
			.mdl-layout__header__link:hover {
			color: #fff; // links
			}
			
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
	<span  style="margin-top: 30px; margin-left: 40px; position: fixed" class="mdl-layout-title mdl-layout--large-screen-only"> <img src="{{URL::asset('img/logo/logo_almenara.jpg')}}" height="30%" width="180px" style="padding: 0px"></span>
        <div class="flex-center position-ref full-height">		
            <div class="top-right links">
                @guest
                    <a href="{{ route('login') }}">Login</a>
                @else
                    <div class="navbar-collapse" id="app-navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="nav_item dropdown">
                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Sair
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    <li>
                                        <a href="{{ route('register') }}">
                                            Registrar
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                @endguest
            </div>

            <div class="content">
                <div class="title m-b-md">
                    IFNMG - Estágio
                </div>
                <div class="links" style="padding-top: 10px">
                    @can('user.admin')
                        <a href="{{ route('documento.indexPublic') }}">Documentos</a>
                        <a href="{{ route('curso.index') }}">Cursos</a>
                        <a href="{{ route('aluno.index') }}">Alunos</a>
                        <a href="{{ route('professor.index') }}">Professores</a>
                        <a href="{{ route('convenio.indexPublic') }}">Convênio</a>
                        <a href="{{ route('estagio.index') }}">Estágio</a>
                    @else
                        <a href="{{ route('convenio.indexPublic') }}">Convênio</a>
                        <a href="{{ route('documento.indexPublic') }}">Documentos</a>
                    @endcan
                </div>
            </div>
        </div>
    </body>
</html>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>