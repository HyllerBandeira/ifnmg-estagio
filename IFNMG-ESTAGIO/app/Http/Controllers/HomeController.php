<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Gate::allows('user.admin')) {
            return redirect()->route('aluno.index')
                        ->with('success','Bem vindo '.Auth::user()->name);
        }
        return redirect()->route('aluno.meusEstagios')
                    ->with('success','Bem vindo '.Auth::user()->name);
    }
}
