<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alunos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aluno', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pessoa_id');
            $table->unsignedInteger('curso_id');
			$table->foreign('pessoa_id')->references('id')->on('pessoa')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('curso_id')->references('id')->on('curso')->onUpdate('cascade');
            $table->string('matricula')->nullable();
            $table->string('foto')->nullable();
            $table->string('turma');
            $table->string('situacao_estagio')->nullable();
            $table->enum('ficha_matricula' ,['true','false'])->nullable();
            $table->string('access')->default('almenara')->notNull();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
