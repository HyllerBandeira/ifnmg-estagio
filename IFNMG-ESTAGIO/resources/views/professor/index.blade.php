@extends('layouts.app')
 
@section('content')
<div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-9">Listagem de Professores</div>
                            @can('user.admin')
                                <div class="text-right col-md-3"><a class="btn btn-default btn-sm" href="{{ route('professor.create') }}" role="button">Novo Professor</a></div>
                            @endcan
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-ordering" >
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th width="200px">Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($professores as $key => $professor)
                                    <tr>
                                        <td>{{ $professor->nome }}</td>
                                        <td>
                                            <a class="btn btn-default btn-sm" href="{{ route('professor.show',$professor->id) }}">Abrir</a>
                                            <a class="btn btn-primary btn-sm" href="{{ route('professor.edit',$professor->id) }}">Editar</a>
                                            {!! Form::open(['method' => 'DELETE','route' => ['professor.destroy', $professor->id],'style'=>'display:inline','class'=>'form-delete-cofirm', 'data-confirmed'=>'false']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection