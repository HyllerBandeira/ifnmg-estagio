<!-- form.blade.php -->

@extends('layouts.pdf')
@section('content')
	<div class="row" style="font-size: 14">

		<div class="col col-12 text-center">
			Ministério da Educação <br>
			Secretaria de Educação Profissional e Tecnológica<br>
			Instituto Federal do Norte de Minas Gerais – Campus Almenara<br>
		</div>
<!-- 
		<div class="col col-2">
			<img src="img/certificado/logo.jpg" style="height: 1.343cm"> 
		</div>
		<div class="col col-1">
			<img src="img/certificado/brasao.png" style="height: 1.8288cm"> 
		</div> -->
	</div>
	<div class="row" style="height: 1.61in">
		&nbsp;
	</div>
	<div class="row" style="width: 100%; font-size: 18">
		<div class="col col-12 text-center">
			<b>DECLARAÇÃO DE CONCLUSÃO DO ESTÁGIO</b>
		</div>
	</div>
	<div class="row" style="height: 3.22in">
		&nbsp;
	</div>
	<div class="row" style="width: 100%; font-size: .40cm">
		<div class="col-12 text-justify">
			Declaro, para os devidos fins e efeitos, que Thatiane Fernandes dos Santos, estudante do curso Análise e Desenvolvimento de Sistemas, realizou o estágio no Centro Estadual de Atenção Especializada, no período de 21/08/2017 a 29/09/2017, cumprindo o total de 224 horas de estágio.

			Declaro, para os devidos fins e efeitos, que o Professor <b> {{ '$professor->nome'}} </b>, professor do Instituto Federal do Norte de Minas – Campus Almenara, realizou atividades de orientação de estágio, a aluna <b>{{ '$aluno->nome' }}</b>, do curso {{ '$curso->nome' }}.
		</div>
	</div>
	<div class="row" style="width: 100%; font-size: .40cm">
		<div class="col-12 text-right">
			Almenara, 13 de março de 2018
		</div>
	</div>
@endsection