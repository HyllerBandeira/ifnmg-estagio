@extends('layouts.default')
 
@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Pessoa</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('pessoa.index') }}"> Voltar</a>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Opa!</strong> Tem algo de errado com as suas entradas.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($pessoa, ['method' => 'PATCH','route' => ['pessoa.update', $pessoa->id]]) !!}
    <div class="row">

		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Tipo:</strong>
				{!! Form::select('tipo', array('aluno'=>'Aluno', 'professor'=>'Professor'), $pessoa->tipo, array('class' => 'form-control')) !!}
			</div>
		</div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nome:</strong>
                {!! Form::text('nome', $pessoa->nome, array('placeholder' => 'Nome','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>RG:</strong>
                {!! Form::text('rg', $pessoa->rg, array('placeholder' => 'RG','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>CPF:</strong>
                {!! Form::text('cpf', $pessoa->cpf, array('placeholder' => 'CPF','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nascimento:</strong>
                {!! Form::text('nascimento', $pessoa->nascimento, array('placeholder' => 'Nascimento','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Logradouro:</strong>
                {!! Form::text('endereco', $pessoa->endereco, array('placeholder' => 'Logradouro','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nº:</strong>
                {!! Form::text('numero', $pessoa->numero, array('placeholder' => 'Nº','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Complemento:</strong>
                {!! Form::text('complemento', $pessoa->complemento, array('placeholder' => 'Complemento','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Bairro:</strong>
                {!! Form::text('bairro', $pessoa->bairro, array('placeholder' => 'Bairro','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Cidade:</strong>
                {!! Form::text('cidade', $pessoa->cidade, array('placeholder' => 'Cidade','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>UF:</strong>
                {!! Form::text('uf', $pessoa->uf, array('placeholder' => 'UF','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>CEP:</strong>
                {!! Form::text('cep', $pessoa->cep, array('placeholder' => 'CEP','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Celular:</strong>
                {!! Form::text('celular', $pessoa->celular, array('placeholder' => 'Celular','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Telefone:</strong>
                {!! Form::text('telefone', $pessoa->telefone, array('placeholder' => 'Telefone','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Salvar</button>
        </div>

    </div>
    {!! Form::close() !!}

@endsection

@section('script')
    <script>
        $('input[name="nascimento"]').datepicker({
            calendarWeeks: false,
            language: "pt-BR",
            format: "dd/mm/yyyy"
        });
        $('input[name="telefone"]').mask('(00) 0000-0000');
        $('input[name="celular"]').mask('(00) 00000-0000');
        $('input[name="cep"]').mask('00000-000');
        $('input[name="uf"]').mask('SS');
        $('input[name="cpf"]').mask('000.000.000-00');
    </script>
@endsection