@extends('layouts.default')
 
@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Gerenciamento de Pessoas</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('pessoa.create') }}"> Adicionar Nova Pessoa</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered table-ordering" >
        <tr>
            <th>Nome</th>
            <th>CPF</th>
            <th width="200px">Ação</th>
        </tr>
    @foreach ($pessoas as $key => $pessoa)
    <tr>
        <td>{{ $pessoa->nome }}</td>
        <td>{{ $pessoa->cpf }}</td>
        <td>
            <a class="btn btn-info" href="{{ route('pessoa.show',$pessoa->id) }}">Abrir</a>
            <a class="btn btn-primary" href="{{ route('pessoa.edit',$pessoa->id) }}">Editar</a>
            {!! Form::open(['method' => 'DELETE','route' => ['pessoa.destroy', $pessoa->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>


@endsection