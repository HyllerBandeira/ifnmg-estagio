@extends('layouts.app')
 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2> Visualização de Aluno</h2>
                    </div>
                    <a href="{{ route('aluno.index') }}">
                        <div class="btn btn-danger pull-right">
                            Voltar
                        </div>
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-5 col-sm-5 col-md-4">
                    <div class="form-group">
                        <strong>Nome:</strong>
                        {{ $aluno->pessoa->nome }}
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <strong>Nascimento:</strong>
                        {{ $aluno->pessoa->nascimento }}
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-4">
                    <div class="form-group">
                        <strong>Situação do Estágio:</strong>
                        {{ $aluno->situacao_estagio }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5 col-sm-5 col-md-4">
                    <div class="form-group">
                        <strong>Email:</strong>
                        {{ $aluno->pessoa->email }}
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="form-group">
                        <strong>Celular:</strong>
                        {{ $aluno->pessoa->celular }}
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-4 letraV">
                    <div class="form-group">
                        <strong>Total Horas Cumpridas:</strong>
                        {{ $aluno->carga_horaria }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="form-group">
                        <strong>CPF:</strong>
                        {{ $aluno->pessoa->cpf }}
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-2">
                    <div class="form-group">
                        <strong>Curso:</strong>
                        {{ $aluno->curso->nome }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-9">Lista de Estágio</div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped " table-paging="false" table-max-height="100">
                            <thead>
                                <tr>
                                    <th>Convênio</th>
                                    <th>Orientador</th>
                                    <th width="150px">Horas Cumpridas</th>
                                    <th width="60px">Ação</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($aluno->estagios()->get() as $estagio)
                                <tr>
                                    <td>{{ $estagio->convenio->nome }}</td>
                                    <td>{{ $estagio->professor->nome }}</td>
                                    <td>{{ $estagio->carga_horaria }}</td>
                                    <td>
                                        <button name="estagio-modal" data-toggle="modal" data-target="#estagio-{{$estagio->id}}-modal" type="button" class="btn btn-default btn-sm">Expandir</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-5">
                            <div class="form-group">
                                <strong>Total Horas Cumpridas:</strong>
                                {{ $aluno->carga_horaria }}
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('modal')

    @foreach ($aluno->estagios()->get() as $estagio)
        <div class="modal fade" id="estagio-{{$estagio->id}}-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content"></div>
            </div>
            <div class="modal-dialog">
                <div class="modal-content"></div>
            </div>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div id="modal-participar" class="modal-header bg-danger">
                        <h4 class="modal-title" id="myModalLabel">{{$estagio->convenio->nome}}</h4>
                        <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span></button>
                    </div>
                    <div class="modal-body">
                        <div calss="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="col-sm-7">
                                    <strong>Termo de Compromisso:</strong>
                                </div>
                                <div class="col-sm-5">
                                    @if($estagio->termo_compromisso == 'true')
                                        <a class='btn btn-success' href="{{route('estagio.pdf',[$estagio->id,'termo_compromisso'])}}" target="_blank">Informado</a>
                                    @else
                                        {!! Form::button('Não informado', array('class' => 'btn btn-danger')) !!}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div calss="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="col-sm-7">
                                    <strong>Plano de Estágio:</strong>
                                </div>
                                <div class="col-sm-5">
                                    @if($estagio->plano_estagio == 'true')
                                        <a class='btn btn-success' href="{{route('estagio.pdf',[$estagio->id,'plano_estagio'])}}" target="_blank">Informado</a>
                                    @else
                                        <button name="estagio-modal" data-toggle="modal" data-target="#estagio-nao-informado-modal" type="button" class="btn btn-danger">Não informado</button>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div calss="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="col-sm-7">
                                    <strong>Controle de Frequência:</strong>
                                </div>
                                <div class="col-sm-5">
                                    @if($estagio->controle_frequencia == 'true')
                                        <a class='btn btn-success' href="{{route('estagio.pdf',[$estagio->id,'controle_frequencia'])}}" target="_blank">Informado</a>
                                    @else
                                        <button name="estagio-modal" data-toggle="modal" data-target="#estagio-nao-informado-modal" type="button" class="btn btn-danger">Não informado</button>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div calss="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="col-sm-7">
                                    <strong>Relatório de Estágio (Convênio):</strong>
                                </div>
                                <div class="col-sm-5">
                                    @if($estagio->relatorio_estagiario == 'true')
                                        <a class='btn btn-success' href="{{route('estagio.pdf',[$estagio->id,'relatorio_estagiario'])}}" target="_blank">Informado</a>
                                    @else
                                        <button name="estagio-modal" data-toggle="modal" data-target="#estagio-nao-informado-modal" type="button" class="btn btn-danger">Não informado</button>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div calss="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="col-sm-7">
                                    <strong>Relatório de Estágio (Aluno):</strong>
                                </div>
                                <div class="col-sm-5">
                                    @if($estagio->relatorio_concedente == 'true')
                                        <a class='btn btn-success' href="{{route('estagio.pdf',[$estagio->id,'relatorio_concedente'])}}" target="_blank">Informado</a>
                                    @else
                                        <button name="estagio-modal" data-toggle="modal" data-target="#estagio-nao-informado-modal" type="button" class="btn btn-danger">Não informado</button>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div calss="row">
                            <div class="col-md-11 col-md-offset-1">
                                <div class="col-sm-7">
                                    <strong>Avaliação do Estágio:</strong>
                                </div>
                                <div class="col-sm-5">
                                    @if($estagio->avaliacao_estagiario == 'true')
                                        <a class='btn btn-success' href="{{route('estagio.pdf',[$estagio->id,'avaliacao_estagiario'])}}" target="_blank">Informado</a>
                                    @else
                                        <button name="estagio-modal" data-toggle="modal" data-target="#estagio-nao-informado-modal" type="button" class="btn btn-danger">Não informado</button>
                                    @endif
                                </div>
                            </div>
                        </div>
                        .
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <div class="modal fade" id="estagio-nao-informado-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content"></div>
        </div>
        <div class="modal-dialog">
            <div class="modal-content"></div>
        </div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div id="modal-participar" class="modal-header bg-danger">
                    <h4 class="modal-title" id="myModalLabel">Documento Não Informado</h4>
                    <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span></button>
                </div>
                <div class="modal-body">
                    <div calss="row">
                        <div class="col-md-11 col-md-offset-1">
                            <strong>Por favor entregue os documentos que faltam ao setor de estágio.</strong>
                        </div>
                    </div>
                </div>
                .
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
@endsection