@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Adicionar novo Professor</h2>
            </div>
            <a href="{{ route('professor.index') }}">
                <div class="btn btn-danger pull-right">
                    Voltar
                </div>
			</a>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Opa!</strong> Tem algo de errado com as suas entradas.<br><br>
            
        </div>
    @endif
    {!! Form::open(array('route' => 'professor.store','method'=>'POST')) !!}

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <label>Campos com <span class="mdi mdi-asterisk" style="color: red"></span> são obrigatórios</label>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="nome">
                    <strong>Nome<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('nome'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('nome') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('nome', null, array('placeholder' => 'Professor','class' => 'form-control', 'maxlength' => 100)) !!}
            </div>
        </div>
        
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<label for="email">
                    <strong>Email<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('email'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('email') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
				{!! Form::email('email', null, array('multiple' => 'multiple', 'class' => 'form-control')) !!}
			</div>
		</div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-success"> ENVIAR </button>
        </div>

    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('script')
    <script>
        $('select[name="curso_id[]"]').select2();
    </script>
@endsection