@extends('layouts.app')
 
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-9">Listagem de Documentos</div>
                            @can('user.admin')
                                <div class="text-right col-md-3"><a class="btn btn-default btn-sm" href="{{ route('documento.create') }}" role="button">Novo Documento</a></div>
                            @endcan
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-ordering" table-paging="false" >
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th class="hidden-sm-down">Descrição</th>
                                        <th width="230px">Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($documentos as $key => $documento)
                                        <tr>
                                            <td>{{ $documento->nome }}</td>
                                            <td class="hidden-sm-down">{{ (strlen($documento->descricao)>100)?substr($documento->descricao,0,97).'...': $documento->descricao }}</td>
                                            <td class=" text-center">
                                                <a class="btn btn-default btn-sm" target="_blank" href="{{ route('documento.abrir',$documento->arquivo) }}">Visualizar</a>
                                                <a class="btn btn-default btn-sm" target="_blank" href="{{ route('documento.abrir',$documento->arquivo_pdf) }}">PDF</a>
                                                @can('user.admin')
                                                    <a class="btn btn-primary btn-sm" href="{{ route('documento.edit',$documento->id) }}">Editar</a>
                                                    {!! Form::open(['method' => 'DELETE','route' => ['documento.destroy', $documento->id],'style'=>'display:inline','class'=>'form-delete-cofirm', 'data-confirmed'=>'false']) !!}
                                                    {!! Form::submit('Excluir', ['class' => 'btn btn-danger btn-sm']) !!}
                                                    {!! Form::close() !!}
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection