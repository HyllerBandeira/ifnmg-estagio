<!DOCTYPE html>

<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="{{url('/img/logoFinal.png')}}" type="image/png" />


        <title>IFNMG - Estágio</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
        
        
        <!-- BOOTSTRAP DATEPICKER -->
        <link href="{{ asset('css/components/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
        <!--/ BOOTSTRAP DATEPICKER -->

        <!-- SELECT2 COM BUSCA -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" /> 
        <!--/ SELECT2 COM BUSCA -->
	
	
	
	
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.light_green-red.min.css">
	<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
	
    </head>
    <body>
        <!-- Always shows a header, even in smaller screens. -->
		
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
	<style>
	    .mdl-layout__header {
		background: #ddd;
		color: #000;
	    }
	    .mdl-layout__header__link:hover {
   		color: #fff; // links
	   }
	</style>
    
    <header class="mdl-layout__header">
  
        <div class="mdl-layout__header-row">	
	
        <!-- Title -->
        <span class="mdl-layout-title mdl-layout--large-screen-only"> <img src="{{URL::asset('img/loogo.png')}}" height="30%" width="225x"></span>
        <!-- Add spacer, to align navigation to the right -->
        <div class="mdl-layout-spacer"></div>
        <!-- Navigation. We hide it in small screens. -->
        <nav class="mdl-navigation mdl-layout--large-screen-only">
            @auth
                <a class="mdl-navigation__link" href="{{ route('documento.indexPublic') }}">Documentos</a>
                <a class="mdl-navigation__link" href="{{ route('curso.index') }}">Cursos</a>
                <a class="mdl-navigation__link" href="{{ route('professor.index') }}">Professores</a>
                <a class="mdl-navigation__link" href="{{ route('convenio.indexPublic') }}">Convênios</a>
                <a class="mdl-navigation__link" href="{{ route('aluno.index') }}">Alunos</a>
                <a class="mdl-navigation__link" href="{{ route('estagio.index') }}">Estágio</a>

                <button id="demo-menu-top-right"
                class="mdl-button mdl-js-button mdl-button--icon">
                    <i class="material-icons">more_vert</i>
                </button>

                <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="demo-menu-top-right">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <li class="mdl-menu__item"> Sair</li>
                    </a>
                    <a href="{{ route('register') }}">
                        <li class="mdl-menu__item"> Registrar</li>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </ul>
            @else
                <a class="mdl-navigation__link" href="{{ route('documento.indexPublic') }}">Documentos</a>
                <a class="mdl-navigation__link" href="{{ route('convenio.indexPublic') }}">Convênios</a>
                
                <button id="demo-menu-top-right"
                class="mdl-button mdl-js-button mdl-button--icon">
                    <i class="material-icons">more_vert</i>
                </button>

                <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" data-mdl-for="demo-menu-top-right">
                    <a href="{{ route('login') }}">
                        <li class="mdl-menu__item">Entrar</li>
                    </a>
                </ul>
        @endauth
        </nav>
    </div>
  </header>
  <div class="mdl-layout__drawer mdl-layout--small-screen-only">
    <span class="mdl-layout-title"><img src="{{URL::asset('img/logo3.png')}}" height="30%" width="200x"></span>
    <nav class="mdl-navigation">
        @auth
            <a class="mdl-navigation__link" href="{{ route('documento.indexPublic') }}">Documentos</a>
            <a class="mdl-navigation__link" href="{{ route('curso.index') }}">Cursos</a>
            <a class="mdl-navigation__link" href="{{ route('aluno.index') }}">Alunos</a>
            <a class="mdl-navigation__link" href="{{ route('professor.index') }}">Professores</a>
            <a class="mdl-navigation__link" href="{{ route('convenio.indexPublic') }}">Convênios</a>
            <a class="mdl-navigation__link" href="{{ route('estagio.index') }}">Estágio</a>

            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <li class="mdl-navigation__link"> Sair</li>
            </a>
            <a href="{{ route('register') }}">
                <li class="mdl-navigation__link"> Registrar</li>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        @else
            <a class="mdl-navigation__link" href="{{ route('documento.indexPublic') }}">Documentos</a>
            <a class="mdl-navigation__link" href="{{ route('convenio.indexPublic') }}">Convênios</a>
            <a href="{{ route('login') }}">
                <li class="mdl-navigation__link">Entrar</li>
            </a>
        @endauth
    </nav>
  </div>
  
   <main class="mdl-layout__content">
    <div class="page-content"><!-- Your content goes here -->
	
	 <div class="container">
            @yield('content')
        </div>
        
        <script src="{{ asset('js/app.js')}}"></script>

        <!-- SELECT2 COM BUSCA -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        <!--/ SELECT2 COM BUSCA -->

        <!-- BOOTSTRAP DATEPICKER -->
        <script src="{{ asset('js/components/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{ asset('js/components/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js')}}"></script>
        <!--/ BOOTSTRAP DATEPICKER -->
        
        <!-- JQUERY MASK -->
        <script src="{{ asset('js/components/jquery-mask/jquery.mask.js')}}"></script>
        <!--/ JQUERY MASK -->

        @yield('script')
	
	</div>
  </main>
</div>
        
       
        @yield('modal')
    </body>
</html>
