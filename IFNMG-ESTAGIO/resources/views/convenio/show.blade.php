@extends('layouts.app')
 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Visualização de Convênio</h2>
            </div>
            <a href="{{ route('convenio.indexPublic') }}">
                <div class="btn btn-danger pull-right">
                    Voltar
                </div>
			</a>
        </div>
    </div>

    <div id="form-fisica">
        @can('user.admin')
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="form-group">
                        <strong>Tipo:</strong>
                        {{ $convenio->tipo }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Nome:</strong>
                        {{ $convenio->nome }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Habilitação Profissional:</strong>
                        {{ $convenio->cargo_habilitacao }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Ramo de Atividade:</strong>
                        {{ $convenio->ramo_atividade }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Conselho de Classe:</strong>
                        {{ $convenio->conselho_classe }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Número Registro:</strong>
                        {{ $convenio->numero_registro }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>CPF:</strong>
                        {{ $convenio->cpf_cnpj }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Endereço:</strong>
                        {{ $convenio->endereco }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Número:</strong>
                        {{ $convenio->numero }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Complemento:</strong>
                        {{ $convenio->complemento }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Bairro:</strong>
                        {{ $convenio->bairro }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Cidade:</strong>
                        {{ $convenio->cidade }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>UF:</strong>
                        {{ $convenio->estado }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>CEP:</strong>
                        {{ $convenio->cep }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Telefone:</strong>
                        {{ $convenio->telefone }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Fax:</strong>
                        {{ $convenio->fax }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>E-mail:</strong>
                        {{ $convenio->email }}
                    </div>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Nome:</strong>
                        {{ $convenio->nome }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Ramo de Atividade:</strong>
                        {{ $convenio->ramo_atividade }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Endereço:</strong>
                        {{ $convenio->endereco }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Número:</strong>
                        {{ $convenio->numero }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Complemento:</strong>
                        {{ $convenio->complemento }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Bairro:</strong>
                        {{ $convenio->bairro }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Cidade:</strong>
                        {{ $convenio->cidade }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>UF:</strong>
                        {{ $convenio->estado }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>CEP:</strong>
                        {{ $convenio->cep }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Telefone:</strong>
                        {{ $convenio->telefone }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Fax:</strong>
                        {{ $convenio->fax }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>E-mail:</strong>
                        {{ $convenio->email }}
                    </div>
                </div>
            </div>
        @endcan
    </div>

    <div id="form-juridica" style="display:none;">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Tipo:</strong>
                    {{ $convenio->tipo }}
                </div>
            </div>
        </div>
        @can('user.admin')
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Razão Social:</strong>
                        {{ $convenio->nome }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Nome Fantasia:</strong>
                        {{ $convenio->nome_fantasia }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Ramo de Atividade:</strong>
                        {{ $convenio->ramo_atividade }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>CNPJ:</strong>
                        {{ $convenio->cpf_cnpj }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Endereço:</strong>
                        {{ $convenio->endereco }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Número:</strong>
                        {{ $convenio->numero }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Complemento:</strong>
                        {{ $convenio->complemento }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Bairro:</strong>
                        {{ $convenio->bairro }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Cidade:</strong>
                        {{ $convenio->cidade }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>UF:</strong>
                        {{ $convenio->estado }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>CEP:</strong>
                        {{ $convenio->cep }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Telefone:</strong>
                        {{ $convenio->telefone }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Fax:</strong>
                        {{ $convenio->fax }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>E-mail:</strong>
                        {{ $convenio->email }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Representante:</strong>
                        {{ $convenio->representante_legal }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Cargo do Representante:</strong>
                        {{ $convenio->cargo_habilitacao }}
                    </div>
                </div>
            </div>

        @else
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Razão Social:</strong>
                        {{ $convenio->nome }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Ramo de Atividade:</strong>
                        {{ $convenio->ramo_atividade }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Endereço:</strong>
                        {{ $convenio->endereco }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Número:</strong>
                        {{ $convenio->numero }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Complemento:</strong>
                        {{ $convenio->complemento }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Bairro:</strong>
                        {{ $convenio->bairro }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Cidade:</strong>
                        {{ $convenio->cidade }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>UF:</strong>
                        {{ $convenio->estado }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>CEP:</strong>
                        {{ $convenio->cep }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Telefone:</strong>
                        {{ $convenio->telefone }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Fax:</strong>
                        {{ $convenio->fax }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>E-mail:</strong>
                        {{ $convenio->email }}
                    </div>
                </div>
            </div>
        @endcan
    </div>
    <div class="col-md-2">
        <div class="row">&nbsp;</div>
        <a class='btn btn-default' href="{{route('convenio.pdf', $convenio->id)}}" target="_blank">Visualizar</a>
    </div>

</div>
@endsection

@section('script')
<script>
    var alteraTipo = function(tipo) {
        $('#form-fisica').show();
        $('#form-juridica').hide();

        if(tipo=='juridica') {
            $('#form-juridica').show();
            $('#form-fisica').hide();
        }
    };
    
	alteraTipo({!!"'".$convenio->tipo."'"!!});
	$(document).ready(function() {
	})
</script>
@endsection