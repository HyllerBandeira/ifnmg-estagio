@extends('layouts.app')
 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2> Visualização de Aluno</h2>
                    </div>
                    <a href="{{ route('aluno.index') }}">
                        <div class="btn btn-danger pull-right">
                            Voltar
                        </div>
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group">
                        <strong>Nome:</strong>
                        {{ $aluno->pessoa->nome }}
                    </div>
                </div>

                <div class="col-xs-6 col-sm-3 col-md-3">
                    <div class="form-group">
                        <strong>Cidade Natal:</strong>
                        {{ $aluno->pessoa->cidade_natal }}
                    </div>
                </div>

                <div class="col-xs-6 col-sm-3 col-md-3">
                    <div class="form-group">
                        <strong>UF Cidade Natal:</strong>
                        {{ $aluno->pessoa->uf_cidade_natal }}
                    </div>
                </div>

                <div class="col-xs-6 col-sm-3 col-md-3">
                    <div class="form-group">
                        <strong>Nascimento:</strong>
                        {{ $aluno->pessoa->nascimento }}
                    </div>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <strong>Filiação:</strong>
                        {{ $aluno->pessoa->filiacao }}
                    </div>
                </div>

                <div class="col-xs-6 col-sm-3 col-md-3">
                    <div class="form-group">
                        <strong>Estado Civil:</strong>
                        {{ $aluno->pessoa->estado_civil }}
                    </div>
                </div>

                <div class="col-xs-6 col-sm-2 col-md-2">
                    <div class="form-group">
                        <strong>Sexo:</strong>
                        {{ $aluno->pessoa->sexo }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="form-group">
                        <strong>RG:</strong>
                        {{ $aluno->pessoa->rg }}
                    </div>
                </div>

                <div class="col-xs-6 col-sm-3 col-md-3">
                    <div class="form-group">
                        <strong>Orgão Expeditor:</strong>
                        {{ $aluno->pessoa->orgao_expeditor }}
                    </div>
                </div>

                <div class="col-xs-6 col-sm-3 col-md-3">
                    <div class="form-group">
                        <strong>CPF:</strong>
                        {{ $aluno->pessoa->cpf }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="form-group">
                        <strong>Celular:</strong>
                        {{ $aluno->pessoa->celular }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="form-group">
                        <strong>Telefone:</strong>
                        {{ $aluno->pessoa->telefone }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="form-group">
                        <strong>Email:</strong>
                        {{ $aluno->pessoa->email }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-5">
                    <div class="form-group">
                        <strong>Curso:</strong>
                        {{ $aluno->curso->nome }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-2">
                    <div class="form-group">
                        <strong>Matricula:</strong>
                        {{ $aluno->matricula }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-5">
                    <div class="form-group">
                        <strong>Turma:</strong>
                        {{ $aluno->turma }}
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-md-3 col-md-offset-3">
                    <div class="row ">&nbsp;</div>
                    <a class='btn btn-default' href="{{route('aluno.pdf',[$aluno->id,'ficha_matricula'])}}" target="_blank">Visualizar Ficha Matrícula</a>
                </div>
                <div class="col-md-3">
                    <div class="row ">&nbsp;</div>
                    <a class='btn btn-default' href="{{route('aluno.foto',[$aluno->id,$aluno->foto])}}" target="_blank">Visualizar Foto Do Aluno</a>
                </div>
            </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-9">Lista de Estágio</div>
                        @can('user.admin')
                            <div class="text-right col-md-3"><a class="btn btn-default btn-sm" href="{{ route('estagio.create()',$aluno->id) }}" role="button">Novo Estágio</a></div>
                        @endcan
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-ordering" table-paging="false" table-max-height="100">
                            <thead>
                                <tr>
                                    <th>Convênio</th>
                                    <th>Orientador</th>
                                    <th width="150px">Horas Cumpridas</th>
                                    <th width="200px">Ação</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($estagios as $key => $estagio)
                                <tr>
                                    <td>{{ $estagio->convenio->nome }}</td>
                                    <td>{{ $estagio->professor->nome }}</td>
                                    <td>{{ $estagio->carga_horaria }}</td>
                                    <td>
                                        <a class="btn btn-default btn-sm" href="{{ route('estagio.show',$estagio->id) }}">Abrir</a>
                                        <a class="btn btn-primary btn-sm" href="{{ route('estagio.edit',$estagio->id) }}">Editar</a>
                                        {!! Form::open(['method' => 'DELETE','route' => ['estagio.destroy()', $estagio->id, $aluno->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection