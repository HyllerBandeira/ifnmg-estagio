<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documentos extends Model
{
    protected $table = 'documento';
    public $fillable = ['id','nome','descricao','arquivo', 'arquivo_pdf', 'access'];
    protected $hidden = ['access'];
}
