@extends('layouts.app')
 
@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Visualização de Estágio</h2>
            </div>
             <a href="{{ route('estagio.index') }}">
                <div class="btn btn-danger pull-right">
                    Voltar
                </div>
			</a>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nome:</strong>
                {{ $estagio->aluno->pessoa->nome }}
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>RG:</strong>
                {{ $estagio->aluno->pessoa->rg }}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>CPF:</strong>
                {{ $estagio->aluno->pessoa->cpf }}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Celular:</strong>
                {{ $estagio->aluno->pessoa->celular }}
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                {{ $estagio->aluno->pessoa->email }}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Curso:</strong>
                {{ $estagio->aluno->curso->nome }}
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Orientador:</strong>
                {{ $estagio->professor->nome }}
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Convênio:</strong>
                {{ $estagio->convenio->nome }}
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Convênio CPF/CNPJ:</strong>
                {{ $estagio->convenio->cpf_cnpj }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Carga Horária Cumprida:</strong>
                {{ $estagio->carga_horaria }}
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Situação:</strong>
                {{ $estagio->situacao }}
            </div>
        </div>

    </div>
@endsection