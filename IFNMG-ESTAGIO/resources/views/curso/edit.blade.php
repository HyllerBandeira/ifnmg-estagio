@extends('layouts.app')
 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Curso</h2>
            </div>
            <div class="pull-right">
                <a href="{{ route('curso.index') }}">
                    <div class="btn btn-danger pull-right">
                        Voltar
                    </div>
                </a>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Opa!</strong> Tem algo de errado com as suas entradas.<br><br>
        </div>
    @endif

    {!! Form::model($curso, ['method' => 'PATCH','route' => ['curso.update', $curso->id]]) !!}
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <label>Campos com <span class="mdi mdi-asterisk" style="color: red"></span> são obrigatórios</label>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="nome">
                    <strong>Nome<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('nome'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('nome') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('nome', $curso->nome, array('placeholder' => 'Nome','class' => 'form-control', 'maxlength' => 100)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="carga_horaria">
                    <strong>Carga Horária<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('carga_horaria'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('carga_horaria') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('carga_horaria', $curso->carga_horaria, array('placeholder' => 'Carga Horária','class' => 'form-control focus numbersOnly')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="descricao">
                    <strong>Descrição<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('descricao'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('descricao') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::textarea('descricao', $curso->descricao, array('placeholder' => 'Descrição','class' => 'form-control','style'=>'height:100px')) !!}
            </div>
        </div>

        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-success"> ENVIAR </button>
        </div>

    </div>
    {!! Form::close() !!}
</div>

@endsection