@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Adicionar novo Estágio</h2>
            </div>
            <a href="{{ route('estagio.index') }}">
                <div class="btn btn-danger pull-right">
                    Voltar
                </div>
			</a>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Opa!</strong> Tem algo de errado com as suas entradas.<br><br>
        </div>
    @endif

    {!! Form::open(array('route' => 'estagio.store', 'enctype' =>'multipart/form-data', 'method'=>'POST')) !!}
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <label>Campos com <span class="mdi mdi-asterisk" style="color: red"></span> são obrigatórios</label>
        </div>
		<div class="col-md-6">
            {!! Form::hidden('origem', $origem) !!}
            
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="aluno_id">
                        <strong>Aluno<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                    </label>
                    @if ($errors->has('aluno_id'))
                        <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('aluno_id') }}">
                            <img src="{{url('/img/alert-outline.png')}}"/>
                        </span>
                    @endif
                    {!! Form::select('aluno_id', $alunos, null, array('class' => 'form-control')) !!}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="professor_id">
                        <strong>Orientador<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                    </label>
                    @if ($errors->has('professor_id'))
                        <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('professor_id') }}">
                            <img src="{{url('/img/alert-outline.png')}}"/>
                        </span>
                    @endif
                    {!! Form::select('professor_id', $professores, null, array('class' => 'form-control')) !!}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="convenio_id">
                        <strong>Convênio<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                    </label>
                    @if ($errors->has('convenio_id'))
                        <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('convenio_id') }}">
                            <img src="{{url('/img/alert-outline.png')}}"/>
                        </span>
                    @endif
                    {!! Form::select('convenio_id', $convenios, null, array('class' => 'form-control')) !!}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="carga_horaria">
                        <strong>Carga Horária Cumprida:</strong>
                    </label>
                    @if ($errors->has('carga_horaria'))
                        <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('carga_horaria') }}">
                            <img src="{{url('/img/alert-outline.png')}}"/>
                        </span>
                    @endif
                    {!! Form::number('carga_horaria', null, array('placeholder' => 'Carga Horária Máxima', 'class' => 'form-control')) !!}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Termo de Compromisso:</strong>
                    {!! Form::file('termo_compromisso', array('class' => 'form-control')) !!}
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Plano de Estágio:</strong>
                    {!! Form::file('plano_estagio', array('class' => 'form-control')) !!}
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Controle de Frequência:</strong>
                    {!! Form::file('controle_frequencia', array('class' => 'form-control')) !!}
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Avaliação da Concedente:</strong>
                    {!! Form::file('relatorio_estagiario', array('class' => 'form-control')) !!}
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Avaliação do Estagiário:</strong>
                    {!! Form::file('relatorio_concedente', array('class' => 'form-control')) !!}
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Relatório Final:</strong>
                    {!! Form::file('avaliacao_estagiario', array('class' => 'form-control')) !!}
                </div>
            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
           <button type="submit" class="btn btn-success" style="margin-bottom:15px"> ENVIAR </button>
        </div>
    </div>
    {!! Form::close() !!}
</div>

@endsection

@section('script')
    <script>
        $('select[name="aluno_id"]').select2();
        $('select[name="professor_id"]').select2();
        $('select[name="convenio_id"]').select2();
    </script>
@endsection