<!-- form.blade.php -->
@php
	$mes = [
		'01' => 'Janeiro',
		'02' => 'Fevereiro',
		'03' => 'Março',
		'04' => 'Abril',
		'05' => 'Maio',
		'06' => 'Junho',
		'07' => 'Julho',
		'08' => 'Agosto',
		'09' => 'Setembro',
		'10' => 'Outubro',
		'11' => 'Novembro',
		'12' => 'Dezembro',
	];
@endphp
@extends('layouts.pdf')
@section('content')
	<div class="row" style="font-size: 12">

		<div class="col col-12 text-center">
			<b> Ministério da Educação <br> </b>
			<b> Instituto Federal do Norte de Minas Gerais – IFNMG <br> </b>
			<b> Campus Almenara </b>
		</div>

		<div class="col col-2" style="margin-left: -15px">
			<img src="img/certificado/logo.jpg" style="height: 1.343cm"> 
		</div>
		<div class="col col-9 text-center">
		</div>
		<div class="col col-1" style="float: right">
			<img src="img/certificado/brasao.png" style="height: 1.8288cm"> 
		</div>
	</div>
	<div class="row" style="height: 1.61in">
		&nbsp;
	</div>
	<div class="row" style="width: 100%; font-size: 12">
		<div class="col col-12 text-center">
			<b>DECLARAÇÃO DE ORIENTAÇÃO DE ESTÁGIO SUPERVISIONADO</b>
		</div>
	</div>
	<div class="row" style="height: 2.05in">
		&nbsp;
	</div>
	<div style="padding-left: 1cm; padding-right: 1cm">
		<div class="row" style="width: 100%; font-size: 12;">
			<div class="col-12 text-justify">
				Declaro, para os devidos fins e efeitos, que o Professor <b>{{ @$estagio->professor->nome }}</b>, professor do Instituto Federal do Norte de Minas – Campus Almenara, realizou atividades de orientação de estágio, o(a) aluno (a) <b>{{ @$estagio->aluno->pessoa->nome }}</b>, do curso {{ @$estagio->aluno->curso->nome }}.
			</div>
		</div>
		<div class="row" style="width: 100%; font-size: .40cm">
			<div class="col-12 text-right">
				Almenara, {{ date('d') }} de {{ $mes[date('m')] }} de {{ date('Y') }}
			</div>
		</div>
	</div>
	<div class="row" style="height: 2.05in">
		&nbsp;
	</div>
	<div class="row" style="width: 100%; font-size: 10">
		<div class="col col-12 text-center">
			<img src="img/certificado/assinatura.png" style="height: 1.07cm; border-bottom: -15px"> <br>
			<hr size="3" width="9.17cm" style="margin-top: -0.5em; margin-bottom: 0.5em;">
			<b style="font-size: 12"><i>Marcos Vinícius Montanari</i></b><br>
			Coordenador de Extensão<br>
			IFNMG – Campus Almenara<br>
			<i>Portaria nº 190 D.O.U. de 06/02/2019</i>
		</div>
	</div>
@endsection