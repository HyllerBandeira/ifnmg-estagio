@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Adicionar novo Documento</h2>
                </div>
                <a href="{{ route('documento.indexPublic') }}">
                    <div class="btn btn-danger pull-right">
                        Voltar
                    </div>
                </a>
            </div>
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Opa!</strong> Tem algo de errado com as suas entradas.<br><br>
            </div>
        @endif
        
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif

        {!! Form::open(array('route' => 'documento.store', 'enctype' =>'multipart/form-data', 'method'=>'POST')) !!}
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <label>Campos com <span class="mdi mdi-asterisk" style="color: red"></span> são obrigatórios</label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="nome">
                        <strong>Nome<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                    </label>
                    @if ($errors->has('nome'))
                        <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('nome') }}">
                            <img src="{{url('/img/alert-outline.png')}}"/>
                        </span>
                    @endif
                    {!! Form::text('nome', null, array('placeholder' => 'Nome', 'class' => 'form-control')) !!}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="descricao">
                        <strong>Descrição<span class="mdi mdi-asterisk" style="color: red"></span>: </strong>
                    </label>
                    @if ($errors->has('descricao'))
                        <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('descricao') }}">
                            <img src="{{url('/img/alert-outline.png')}}"/>
                        </span>
                    @endif
                    {!! Form::text('descricao', null, array('placeholder' => 'Descrição', 'class' => 'form-control')) !!}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="arquivo">
                        <strong>Arquivo<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                    </label>
                    @if ($errors->has('arquivo'))
                        <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('arquivo') }}">
                            <img src="{{url('/img/alert-outline.png')}}"/>
                        </span>
                    @endif
                    {!! Form::file('arquivo', array('class' => 'form-control')) !!}
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="arquivo">
                        <strong>Arquivo PDF<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                    </label>
                    @if ($errors->has('arquivo_pdf'))
                        <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('arquivo_pdf') }}">
                            <img src="{{url('/img/alert-outline.png')}}"/>
                        </span>
                    @endif
                    {!! Form::file('arquivo_pdf', array('class' => 'form-control')) !!}
                </div>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-success"> ENVIAR </button>
            </div>

        </div>
    </div>
    {!! Form::close() !!}
@endsection