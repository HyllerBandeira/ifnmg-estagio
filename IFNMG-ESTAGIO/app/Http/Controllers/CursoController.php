<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

use Auth;
use App\Cursos;

class CursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $access = Auth::user()->access ?? env('APP_ACCESS');
        
        $cursos = Cursos::where('access', $access)->orderBy('nome','ASC')->get();
        return view('curso.index',compact('cursos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('curso.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required',
            'descricao' => 'required',
            'carga_horaria' => 'required',
        ],[
            'required' => 'Este campo é obrigatório.'
        ]);
        
        // Adição do controle de acesso
        $request['access'] = Auth::user()->access;

        Cursos::create($request->all());
        return redirect()->route('curso.index')
                        ->with('success','Registro salvo com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  Cursos  $curso
     * @return \Illuminate\Http\Response
     */
    public function show(Cursos $curso)
    {;
        return view('curso.show',compact('curso'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Cursos  $curso
     * @return \Illuminate\Http\Response
     */
    public function edit(Cursos $curso)
    {
        if (Gate::denies('curso.controlar', $curso)) {
            return abort(403, "Você não tem acesso a esta função");
        }
        
        return view('curso.edit',compact('curso'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Cursos  $curso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cursos $curso)
    {
        if (Gate::denies('curso.controlar', $curso)) {
            return abort(403, "Você não tem acesso a esta função");
        }
        
        $this->validate($request, [
            'nome' => 'required',
            'descricao' => 'required',
            'carga_horaria' => 'required',
        ],[
            'required' => 'Este campo é obrigatório.'
        ]);

        $curso->update($request->all());
        return redirect()->route('curso.index')
                        ->with('success','Registro alterado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Cursos  $curso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cursos $curso)
    {
        if (Gate::denies('curso.controlar', $curso)) {
            return abort(403, "Você não tem acesso a esta função");
        }
        
        try{
            
            $curso->delete();
            
        } catch (\PDOException $e) {
            
            return redirect()->route('curso.index')->with('error', 'Não é possível excluir o curso, pois há alunos cadastrado neste curso.');
        }
        
        return redirect()->route('curso.index')
                        ->with('success','Registro deletado com sucesso');
    }
}
