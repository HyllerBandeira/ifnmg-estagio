<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;

use Auth;
use App;
use App\Alunos;
use App\Convenios;
use App\Estagios;
use App\Professores;

class EstagiosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $access = Auth::user()->access ?? env('APP_ACCESS');

        $alunos = Alunos::where('access', $access)->has('estagios')->get();
        return view('estagio.index',compact('alunos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($aluno_id=0)
    {
        $aluno = Alunos::find($aluno_id);
        if($aluno) {
            $alunos[$aluno->id] = "{$aluno->pessoa->nome} - {$aluno->curso->nome}";

            $retorno = Professores::orderBy('id','DESC')->get();
            $professores;
            foreach($retorno as $professor) {
                $professores[$professor->id] = $professor->nome;
            }

            $retorno = Convenios::orderBy('id','DESC')->get();
            $convenios;
            foreach($retorno as $convenio) {
                $convenios[$convenio->id] = $convenio->nome;
            }
            $origem = 'alunos';
        }
        else {

            $retorno = Alunos::orderBy('id','DESC')->get();
            $alunos;
            foreach($retorno as $aluno)
                $alunos[$aluno->id] = "{$aluno->pessoa->nome} - {$aluno->curso->nome}";

            $retorno = Professores::orderBy('id','DESC')->get();
            $professores;
            foreach($retorno as $professor)
                $professores[$professor->id] = $professor->nome;

            $retorno = Convenios::orderBy('id','DESC')->get();
            $convenios;
            foreach($retorno as $convenio)
                $convenios[$convenio->id] = $convenio->nome;
            $origem = 'estagios';
        }
        $error = 0;
        $errorMessage = [];
        if(empty($alunos)) {
            $alunos = [];
            $errorMessage[] = 'Não existem Alunos cadastrados;';
            $error = true;
        }

        if(empty($professores)) {
            $professores = [];
            $errorMessage[] = 'Não existem Professores cadastrados;';
            $error = true;
        }

        if(empty($convenios)) {
            $convenios = [];
            $errorMessage[] = 'Não existem Convênios cadastrados;';
            $error = true;
        }

        if($error)
            return view('estagio.create', ['alunos' => $alunos, 'professores' => $professores, 'convenios' => $convenios, 'origem' => $origem])->withErrors($errorMessage);

        return view('estagio.create', ['alunos' => $alunos, 'professores' => $professores, 'convenios' => $convenios, 'origem' => $origem]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'aluno_id' => 'required',
            'professor_id' => 'required',
            'convenio_id' => 'required',
            'carga_horaria' => 'integer|nullable',
        ],[
            'required' => 'Este campo é obrigatório.',
            'integer' => 'Apenas numeros.'
        ]);


        $estagio_data = [
            'aluno_id' => $request->aluno_id,
            'convenio_id' => $request->convenio_id,
            'professor_id' => $request->professor_id,
            'carga_horaria' => $request->carga_horaria,
            'situacao' => $request->situacao,
            'access' => Auth::user()->access,
        ];

        $estagio = Estagios::create($estagio_data);

        if($request->file('termo_compromisso') != null) {
            $path_termo_compromisso = $request->file('termo_compromisso')
                ->storeAs("estagios/$estagio->id/pdfs","termo_compromisso.pdf");
            $estagio_data['termo_compromisso'] = 'true';
        }

        if($request->file('plano_estagio') != null) {
            $path_plano_estagio = $request->file('plano_estagio')
                ->storeAs("estagios/$estagio->id/pdfs","plano_estagio.pdf");
            $estagio_data['plano_estagio'] = 'true';
        }

        if($request->file('relatorio_estagiario') != null) {
            $path_relatorio_estagiario = $request->file('relatorio_estagiario')
                ->storeAs("estagios/$estagio->id/pdfs","relatorio_estagiario.pdf");
            $estagio_data['relatorio_estagiario'] = 'true';

        }
        if($request->file('relatorio_concedente') != null) {
            $path_relatorio_concedente = $request->file('relatorio_concedente')
                ->storeAs("estagios/$estagio->id/pdfs","relatorio_concedente.pdf");
            $estagio_data['relatorio_concedente'] = 'true';
        }

        if($request->file('avaliacao_estagiario') != null) {
            $path_avaliacao_estagiario = $request->file('avaliacao_estagiario')
                ->storeAs("estagios/$estagio->id/pdfs","avaliacao_estagiario.pdf");
            $estagio_data['avaliacao_estagiario'] = 'true';
        }

        if($request->file('controle_frequencia') != null) {
            $path_controle_frequencia = $request->file('controle_frequencia')
                ->storeAs("estagios/$estagio->id/pdfs","controle_frequencia.pdf");
            $estagio_data['controle_frequencia'] = 'true';
        }

        $estagio->update($estagio_data);
        $estagio->checkStatus();

        if($request['origem'] == 'alunos')
            return redirect()->route('aluno.show',$estagio->aluno_id)
                                ->with('success','Registro adicionado com sucesso');

        return redirect()->route('estagio.index')
                            ->with('success','Registro adicionado com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  Estagios  $estagios
     * @return \Illuminate\Http\Response
     */
    public function show(Estagios $estagio)
    {
        return view('estagio.show',compact('estagio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Estagios  $estagios
     * @param  int  $aluno_id
     * @return \Illuminate\Http\Response
     */
    public function edit(Estagios $estagio, $aluno_id = 0)
    {
        if (Gate::denies('estagio.controlar', $estagio)) {
            return abort(403, "Você não tem acesso a esta função");
        }

        $aluno = Alunos::find($aluno_id);

        if($aluno) {
            $alunos[$aluno->id] = $aluno->pessoa->nome.' - '.$aluno->curso->nome;

            $retorno = Professores::orderBy('id','DESC')->get();
            $professores;
            foreach($retorno as $professor) {
                if($professor->cursos()->find($aluno->curso_id))
                    $professores[$professor->id] = $professor->nome;
            }

            $retorno = Convenios::orderBy('id','DESC')->get();
            $convenios;
            foreach($retorno as $convenio) {
                if($convenio->cursos()->find($aluno->curso_id))
                    $convenios[$convenio->id] = $convenio->nome;
            }
            $origem = 'alunos';
        }
        else {
            $retorno = Alunos::orderBy('id','DESC')->get();
            $alunos;
            foreach($retorno as $aluno)
                $alunos[$aluno->id] = $aluno->pessoa->nome.' - '.$aluno->curso->nome;

            $retorno = Professores::orderBy('id','DESC')->get();
            $professores;
            foreach($retorno as $professor)
                $professores[$professor->id] = $professor->nome;

            $retorno = Convenios::orderBy('id','DESC')->get();
            $convenios;
            foreach($retorno as $convenio)
                $convenios[$convenio->id] = $convenio->nome;
            $origem = 'estagios';
        }
        $error = 0;
        $errorMessage = [];
        if(empty($alunos)) {
            $alunos = [];
            $errorMessage[] = 'Não ecistem alunos cadastrados cadastrados;';
            $error = true;
        }
        if(empty($professores)) {
            $professores = [];
            $errorMessage[] = 'Curso informado não contem Professores cadastrados;';
            $error = true;
        }

        if(empty($convenios)) {
            $convenios = [];
            $errorMessage[] = 'Curso informado não contem Convênios cadastrados;';
            $error = true;
        }

        if($error)
            return view('estagio.create', ['alunos' => $alunos, 'professores' => $professores, 'convenios' => $convenios, 'origem' => $origem])->withErrors($errorMessage);


        return view('estagio.edit', ['alunos' => $alunos, 'professores' => $professores, 'convenios' => $convenios, 'estagio' => $estagio, 'origem' => $origem]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Estagios  $estagios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Estagios $estagio)
    {
        if (Gate::denies('estagio.controlar', $estagio)) {
            return abort(403, "Você não tem acesso a esta função");
        }

        $this->validate($request, [
            'aluno_id' => 'required',
            'professor_id' => 'required',
            'convenio_id' => 'required'
        ],[
            'required' => 'Este campo é obrigatório.'
        ]);

        $estagio_data = [
            'aluno_id' => $request->aluno_id,
            'convenio_id' => $request->convenio_id,
            'professor_id' => $request->professor_id,
            'carga_horaria' => $request->carga_horaria,
            'situacao' => $request->situacao,
        ];

        if($request->file('termo_compromisso')) {
            $path_termo_compromisso = $request->file('termo_compromisso')
                ->storeAs("estagios/$estagio->id/pdfs","termo_compromisso.pdf");
            $estagio_data['termo_compromisso'] = 'true';
        }

        if($request->file('plano_estagio')) {
            $path_plano_estagio = $request->file('plano_estagio')
                ->storeAs("estagios/$estagio->id/pdfs","plano_estagio.pdf");
            $estagio_data['plano_estagio'] = 'true';
        }

        if($request->file('relatorio_estagiario')) {
            $path_relatorio_estagiario = $request->file('relatorio_estagiario')
                ->storeAs("estagios/$estagio->id/pdfs","relatorio_estagiario.pdf");
            $estagio_data['relatorio_estagiario'] = 'true';

        }
        if($request->file('relatorio_concedente')) {
            $path_relatorio_concedente = $request->file('relatorio_concedente')
                ->storeAs("estagios/$estagio->id/pdfs","relatorio_concedente.pdf");
            $estagio_data['relatorio_concedente'] = 'true';
        }

        if($request->file('avaliacao_estagiario')) {
            $path_avaliacao_estagiario = $request->file('avaliacao_estagiario')
                ->storeAs("estagios/$estagio->id/pdfs","avaliacao_estagiario.pdf");
            $estagio_data['avaliacao_estagiario'] = 'true';
        }

        if($request->file('controle_frequencia')) {
            $path_controle_frequencia = $request->file('controle_frequencia')
                ->storeAs("estagios/$estagio->id/pdfs","controle_frequencia.pdf");
            $estagio_data['controle_frequencia'] = 'true';
        }
        $estagio->update($estagio_data);
        $estagio->checkStatus();

        if($request['origem'] == 'alunos')
            return redirect()->route('aluno.show',$estagio->aluno_id)
                ->with('success','Registro alterado com sucesso');

        return redirect()->route('estagio.index')
            ->with('success','Registro alterado com sucesso');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Estagios  $estagios
     * @return \Illuminate\Http\Response
     */
    public function destroy(Estagios $estagio, $aluno_id = 0)
    {
        die(print_r($estagio));
        if (Gate::denies('estagio.controlar', $estagio)) {
            return abort(403, "Você não tem acesso a esta função");
        }

        $estagio->aluno->checkStatus();
        $estagio->delete();

        if($aluno_id != 0)
            return redirect()->route('aluno.show',$aluno_id)
                                ->with('success','Registro removido com sucesso');

        return redirect()->route('estagio.index')
                            ->with('success','Registro removido com sucesso');
    }

    /**
     * Get the specified document from storage.
     *
     * @param  \App\Estagios  $estagios
     * @return \Illuminate\Http\Response
     */
    public function showPdf($estagio_id =0, $documento=0)
    {
        if(($estagio_id < 1))
            abort(403, "Estágio inválido");

        $file_path = "estagios/$estagio_id/pdfs/$documento.pdf";
        if(Storage::disk('local')->exists("/$file_path"))
            return response()->file(storage_path("app/$file_path"));

        abort(404, "Arquivo não enconrado");
    }

    /**
     * Remove the specified document from storage.
     *
     * @param  \App\Estagios  $estagios
     * @return \Illuminate\Http\Response
     */
    public function destroyPdf(Request $request, $id)
    {
        $estagio = Estagios::findOrFail($id);

        if (Gate::denies('estagio.controlar', $estagio)) {
            return abort(403, "Você não tem acesso a esta função");
        }

        $this->validate($request, [
            'documento' => 'required',
        ]);


        $estagio->update([$request->documento => 'false']);
        $estagio->checkStatus();

        $file_path = "estagios/$id/pdfs/$request->documento.pdf";
//        dd(storage_path("app/$file_path"));
//        unlink(storage_path("app/$file_path"));
        Storage::disk('local')->delete("/$file_path");

        return redirect()->route('estagio.edit', $id)
            ->wiithInth('success','Documento removido com sucesso')
            ->wput();
    }
}
