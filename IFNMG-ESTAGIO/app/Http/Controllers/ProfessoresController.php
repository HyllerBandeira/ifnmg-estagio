<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Auth;
use App\Cursos;
use App\Professores;

class ProfessoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $access = Auth::user()->access ?? env('APP_ACCESS');
        
        $professores = Professores::where('access', $access)->orderBy('id','DESC')->get();
            return view('professor.index', compact('professores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('professor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required',
            'email' => 'required|unique:professor',
        ],[
            'required' => 'Este campo é obrigatório.',
            'unique' => 'Este email já está sendo usado por algum professor.'
        ]);

        // Adição do controle de acesso
        $request['access'] = Auth::user()->access;

        $professor = Professores::create($request->all());
        return redirect()->route('professor.index')
                        ->with('success','Registro salvo com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Professores  $professor
     * @return \Illuminate\Http\Response
     */
    public function show(Professores $professor)
    {
        return view('professor.show',compact('professor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Professores  $professor
     * @return \Illuminate\Http\Response
     */
    public function edit(Professores $professor)
    {
        if (Gate::denies('professor.controlar', $professor)) {
            return abort(403, "Você não tem acesso a esta função");
        }
        
        $retorno = Cursos::orderBy('id','DESC')->get();
        $cursos;
        foreach($retorno as $curso) {
            $cursos[$curso->id] = $curso->nome;
        }
        $error = 0;
        $errorMessage = '';
        if(empty($cursos)) {
            $cursos = [];
            $errorMessage[] = 'Não existem cursos cadastrados;';
            $error = true;
        }
        
        if($error)
            return view('professor.edit',['professor' => $professor, 'cursos' => $cursos])->withErrors($errorMessage);
        
        return view('professor.edit',['professor' => $professor, 'cursos' => $cursos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Professores  $professor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Professores $professor)
    {
        if (Gate::denies('professor.controlar', $professor)) {
            return abort(403, "Você não tem acesso a esta função");
        }
        
        $this->validate($request, [
            'nome' => 'required',
            'email' => 'required',
        ],[
            'required' => 'Este campo é obrigatório.',
            'unique' => 'Este email já está sendo usado por algum professor.'
        ]);

        $professor->update($request->all());
        $professor->cursos()->sync($request->input('curso_id'));
        return redirect()->route('professor.index')
                        ->with('success','Registro alterado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Professores  $professor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Professores $professor)
    {
        if (Gate::denies('professor.controlar', $professor)) {
            return abort(403, "Você não tem acesso a esta função");
        }
        
        try{
            
            $professor->cursos()->detach();
            $professor->delete();
            
        } catch (\PDOException $e) {
            
            return redirect()->route('professor.index')->with('error', 'Não é possível excluir o professor, pois ele possui um estágio em que ele é o Orientador.');
        }
        
        return redirect()->route('professor.index')
                        ->with('success','Registro deletado com sucesso');
    }
}
