@extends('layouts.app')
 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Convênio</h2>
            </div>
            <div class="pull-right">
                <a href="{{ route('convenio.indexPublic') }}">
                    <div class="btn btn-danger pull-right">
                        Voltar
                    </div>
                </a>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Opa!</strong> Tem algo de errado com as suas entradas.<br><br>
        </div>
    @endif
	<div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <label>Campos com <span class="mdi mdi-asterisk" style="color: red"></span> são obrigatórios</label>
        </div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<label for="tipo">
                    <strong>Tipo<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('tipo'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('tipo') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
				{!! Form::select('tipo', array('fisica'=>'Fisica', 'juridica'=>'Juridica'), $convenio->tipo, array('id'=>'select-tipo', 'class' => 'form-control')) !!}
			</div>
		</div>
	</div>
	
    {!! Form::model($convenio, ['id'=>'form-fisica', 'enctype' =>'multipart/form-data', 'method' => 'PATCH','route' => ['convenio.update', $convenio->id]]) !!}
    <div class="row">
		{!! Form::hidden('tipo', 'fisica') !!}
		
		<div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <label for="nome">
                    <strong>Nome<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('nome'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('nome') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('nome', $convenio->nome, array('placeholder' => 'Nome','class' => 'form-control', 'maxlength' => 100)) !!}
            </div>
        </div>
		
		<div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <label for="cargo_habilitacao">
                    <strong>Habilitação Profissional<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('cargo_habilitacao'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('cargo_habilitacao') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('cargo_habilitacao', $convenio->cargo_habilitacao, array('placeholder' => 'Habilitação','class' => 'form-control', 'maxlength' => 100)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-5">
            <div class="form-group">
                <label for="ramo_atividade">
                    <strong>Ramo de Atividade<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('ramo_atividade'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('ramo_atividade') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('ramo_atividade', $convenio->ramo_atividade, array('placeholder' => 'Ramo de Atividade','class' => 'form-control', 'maxlength' => 100)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="form-group">
                <label for="conselho_classe">
                    <strong>Conselho de Classe:</strong>
                </label>
                @if ($errors->has('conselho_classe'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('conselho_classe') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('conselho_classe', $convenio->conselho_classe, array('placeholder' => 'Orgão de Classe','class' => 'form-control', 'maxlength' => 100)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-3">
            <div class="form-group">
                <label for="numero_registro">
                    <strong>Número Registro:</strong>
                </label>
                @if ($errors->has('numero_registro'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('numero_registro') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('numero_registro', $convenio->numero_registro, array('placeholder' => 'Número Registro','class' => 'form-control', 'maxlength' => 50)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-3">
            <div class="form-group">
               <label for="cpf_cnpj">
                    <strong>CPF<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('cpf_cnpj'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('cpf_cnpj') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('cpf_cnpj', $convenio->cpf_cnpj, array('placeholder' => 'CPF','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-10">
            <div class="form-group">
                <label for="endereco">
                    <strong>Endereço<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('endereco'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('endereco') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('endereco', $convenio->endereco, array('placeholder' => 'Endereço','class' => 'form-control', 'maxlength' => 50)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-2">
            <div class="form-group">
               <label for="numero">
                    <strong>Número<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('numero'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('numero') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('numero', $convenio->numero, array('placeholder' => 'Número','class' => 'form-control', 'maxlength' => 10)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="complemento">
                    <strong>Complemento:</strong>
                </label>
                @if ($errors->has('complemento'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('complemento') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('complemento', $convenio->complemento, array('placeholder' => 'Complemento','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="form-group">
                <label for="bairro">
                    <strong>Bairro<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('bairro'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('bairro') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('bairro', $convenio->bairro, array('placeholder' => 'Bairro','class' => 'form-control', 'maxlength' => 50)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-5">
            <div class="form-group">
                <label for="cidade">
                    <strong>Cidade<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('cidade'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('cidade') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('cidade', $convenio->cidade, array('placeholder' => 'Cidade','class' => 'form-control', 'maxlength' => 50)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-1">
            <div class="form-group">
                <label for="estado">
                    <strong>UF<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('estado'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('estado') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('estado', $convenio->estado, array('placeholder' => 'UF','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-2">
            <div class="form-group">
                <label for="cep">
                    <strong>CEP<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('cep'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('cep') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('cep', $convenio->cep, array('placeholder' => 'CEP','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="form-group">
                <label for="telefone">
                    <strong>Telefone:</strong>
                </label>
                @if ($errors->has('telefone'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('telefone') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('telefone', $convenio->telefone, array('placeholder' => 'Telefone','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="form-group">
                <label for="fax">
                    <strong>Fax:</strong>
                </label>
                @if ($errors->has('fax'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('fax') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('fax', $convenio->fax, array('placeholder' => 'Fax','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="form-group">
                <label for="email">
                    <strong>E-mail<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('email'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('email') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('email', $convenio->email, array('placeholder' => 'E-mail','class' => 'form-control', 'maxlength' => 100)) !!}
            </div>
        </div>
        
        <div class="col-md-12">
            <div class="form-group">
                <label for="cadastro_convenio">
                    <strong>Ficha de Cadastro<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('cadastro_convenio'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('cadastro_convenio') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::file('cadastro_convenio', array('class' => 'form-control file')) !!}
            </div>
        </div>
        
        <div class="col-md-2">
            <div class="row">&nbsp;</div>
            <a class='btn btn-default' href="{{route('convenio.pdf',[$convenio->id,'cadastro_convenio'])}}" target="_blank">Visualizar</a>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-success"style="margin-bottom:15px"> ENVIAR </button>
        </div>

    </div>
    {!! Form::close() !!}
	
	{!! Form::model($convenio, ['id'=>'form-juridica', 'enctype' =>'multipart/form-data', 'method' => 'PATCH','route' => ['convenio.update', $convenio->id]]) !!}
    <div class="row">
		{!! Form::hidden('tipo', 'juridica') !!}
		
		<div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <label for="nome_fantasia">
                    <strong>Nome Fantasia<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('nome_fantasia'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('nome_fantasia') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('nome_fantasia', $convenio->nome_fantasia, array('placeholder' => 'Nome Fantasia','class' => 'form-control', 'maxlength' => 100)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <label for="nome">
                    <strong>Razão Social<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('nome'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('nome') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('nome', $convenio->nome, array('placeholder' => 'Razão Social','class' => 'form-control', 'maxlength' => 100)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="form-group">
                <label for="ramo_atividade">
                    <strong>Ramo de Atividade<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('ramo_atividade'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('ramo_atividade') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('ramo_atividade', $convenio->ramo_atividade, array('placeholder' => 'Ramo de Atividade','class' => 'form-control', 'maxlength' => 100)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="form-group">
                <label for="cpf_cnpj">
                    <strong>CNPJ<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('cpf_cnpj'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('cpf_cnpj') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('cpf_cnpj', $convenio->cpf_cnpj, array('placeholder' => 'CNPJ','class' => 'form-control')) !!}
            </div>
        </div>

        

        <div class="col-xs-12 col-sm-12 col-md-10">
            <div class="form-group">
                <label for="endereco">
                    <strong>Endereço<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('endereco'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('endereco') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('endereco', $convenio->endereco, array('placeholder' => 'Endereço','class' => 'form-control', 'maxlength' => 50)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-2">
            <div class="form-group">
                <label for="numero">
                    <strong>Número<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('numero'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('numero') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('numero', $convenio->numero, array('placeholder' => 'Número','class' => 'form-control', 'maxlength' => 10)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="complemento">
                    <strong>Complemento:</strong>
                </label>
                @if ($errors->has('complemento'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('complemento') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('complemento', $convenio->complemento, array('placeholder' => 'Complemento','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="form-group">
                <label for="bairro">
                    <strong>Bairro<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('bairro'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('bairro') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('bairro', $convenio->bairro, array('placeholder' => 'Bairro','class' => 'form-control', 'maxlength' => 50)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-5">
            <div class="form-group">
                <label for="cidade">
                    <strong>Cidade<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('cidade'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('cidade') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('cidade', $convenio->cidade, array('placeholder' => 'Cidade','class' => 'form-control', 'maxlength' => 50)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-1">
            <div class="form-group">
                <label for="estado">
                    <strong>UF<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('estado'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('estado') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('estado', $convenio->estado, array('placeholder' => 'UF','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-2">
            <div class="form-group">
                <label for="cep">
                    <strong>CEP<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('cep'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('cep') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('cep', $convenio->cep, array('placeholder' => 'CEP','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="form-group">
                <label for="telefone">
                    <strong>Telefone:</strong>
                </label>
                @if ($errors->has('telefone'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('telefone') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('telefone', $convenio->telefone, array('placeholder' => 'Telefone','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="form-group">
                <label for="fax">
                    <strong>Fax:</strong>
                </label>
                @if ($errors->has('fax'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('fax') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('fax', $convenio->fax, array('placeholder' => 'Fax','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="form-group">
                <label for="email">
                    <strong>E-mail<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('email'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('email') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('email', $convenio->email, array('placeholder' => 'E-mail','class' => 'form-control', 'maxlength' => 100)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <label for="representante_legal">
                    <strong>Representante<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('representante_legal'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('representante_legal') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('representante_legal', $convenio->representante_legal, array('placeholder' => 'Representante','class' => 'form-control', 'maxlength' => 100)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <label for="cargo_habilitacao">
                    <strong>Cargo do Representante<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('cargo_habilitacao'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('cargo_habilitacao') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('cargo_habilitacao', $convenio->cargo_habilitacao, array('placeholder' => 'Cargo do Representante','class' => 'form-control')) !!}
            </div>
        </div>
        
        <div class="col-md-12">
            <div class="form-group">
                <label for="cadastro_convenio">
                    <strong>Ficha de Cadastro<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('cadastro_convenio'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('cadastro_convenio') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::file('cadastro_convenio', array('class' => 'form-control file')) !!}
            </div>
        </div>
        
        <div class="col-md-2">
            <div class="row">&nbsp;</div>
            <a class='btn btn-default' href="{{route('convenio.pdf', $convenio->id)}}" target="_blank">Visualizar</a>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-success"style="margin-bottom:15px"> ENVIAR </button>
        </div>

    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('script')
    <script>
        var alteraTipo = function(tipo) {
            $('#form-fisica').show();
            $('#form-juridica').hide();

            if(tipo=='juridica') {
                $('#form-juridica').show();
                $('#form-fisica').hide();
            }
        };

        $('#select-tipo').change(function() {
            alteraTipo($(this).val());
        });

        alteraTipo($('#select-tipo').val());
        $(document).ready(function() {
        })
        $('input[name="nascimento"]').datepicker({
            calendarWeeks: false,
            language: "pt-BR",
            format: "dd/mm/yyyy"
        });
        $('input[name="fax"]').mask('(00) 0000-0000');
        $('input[name="telefone"]').mask('(00) 0000-0000');
        $('input[name="celular"]').mask('(00) 00000-0000');
        $('input[name="cep"]').mask('00000-000');
        $('input[name="estado"]').mask('SS');
        $('#form-fisica input[name="cpf_cnpj"]').mask('000.000.000-00');
        $('#form-juridica input[name="cpf_cnpj"]').mask('00.000.000/0000-00');
        
        
        
        $("button[name='remover_documento']").on('click', function() {
            var tipo_documento = $(this).data('documento'); 
            $("input[name='documento']").val(tipo_documento);
        });
    </script>
@endsection