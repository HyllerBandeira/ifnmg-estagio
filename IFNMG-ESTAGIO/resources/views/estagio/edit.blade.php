@extends('layouts.app')
 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Estágio</h2>
            </div>
            <a href="{{ route('estagio.index') }}">
                <div class="btn btn-danger pull-right">
                    Voltar
                </div>
			</a>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Opa!</strong> Tem algo de errado com as suas entradas.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($estagio, ['method' => 'PATCH', 'enctype' =>'multipart/form-data', 'route' => ['estagio.update', $estagio->id]]) !!}
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <label>Campos com <span class="mdi mdi-asterisk" style="color: red"></span> são obrigatórios</label>
        </div>
        <div class="col-md-6">
            {!! Form::hidden('origem', $origem) !!}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="aluno_id">
                        <strong>Aluno<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                    </label>
                    @if ($errors->has('aluno_id'))
                        <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('aluno_id') }}">
                            <img src="{{url('/img/alert-outline.png')}}"/>
                        </span>
                    @endif
                    {!! Form::select('aluno_id', $alunos, $estagio->aluno_id, array('class' => 'form-control')) !!}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="professor_id">
                        <strong>Orientador<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                    </label>
                    @if ($errors->has('professor_id'))
                        <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('professor_id') }}">
                            <img src="{{url('/img/alert-outline.png')}}"/>
                        </span>
                    @endif
                    {!! Form::select('professor_id', $professores, $estagio->professor_id, array('class' => 'form-control')) !!}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="convenio_id">
                        <strong>Convênio<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                    </label>
                    @if ($errors->has('convenio_id'))
                        <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('convenio_id') }}">
                            <img src="{{url('/img/alert-outline.png')}}"/>
                        </span>
                    @endif
                    {!! Form::select('convenio_id', $convenios, $estagio->convenio_id, array('class' => 'form-control')) !!}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="carga_horaria">
                        <strong>Carga Horária Cumprida:</strong>
                    </label>
                    @if ($errors->has('carga_horaria'))
                        <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('carga_horaria') }}">
                            <img src="{{url('/img/alert-outline.png')}}"/>
                        </span>
                    @endif
                    {!! Form::number('carga_horaria', $estagio->carga_horaria, array('placeholder' => 'Carga Horária Máxima', 'class' => 'form-control')) !!}
                </div>
            </div>
        </div>

        <div class="col-md-6">
<!--
            <div class="col-md-9">
                <div class="form-group">
                    <strong>Ficha de Matricula de Estagio:</strong>
                    {!! Form::file('ficha_matricula', array('class' => 'form-control file')) !!}
                </div>
            </div>
            @if($estagio->ficha_matricula == 'true')
                <div class="col-md-1">
                    &nbsp;
                    <button name="remover_documento" data-documento="ficha_matricula" data-toggle="modal" data-target="#confirm-modal"  type="button" class="btn btn-danger">x</button>    
                </div>
                
                <div class="col-md-2">
                    &nbsp;
                    <a class='btn btn-default' href="{{route('estagio.pdf',[$estagio->id,'ficha_matricula'])}}" target="_blank">Visualizar</a>
                </div>
            @endif
-->
            
            <div class="col-md-9">
                <div class="form-group">
                    <strong>Termo de Compromisso:</strong>
                    {!! Form::file('termo_compromisso', array('class' => 'form-control file')) !!}
                </div>
            </div>
            @if($estagio->termo_compromisso == 'true')
                <div class="col-md-1">
                    &nbsp;
                    <button name="remover_documento" data-documento="termo_compromisso" data-toggle="modal" data-target="#confirm-modal"  type="button" class="btn btn-danger">x</button>    
                </div>
                
                <div class="col-md-2">
                    &nbsp;
                    <a class='btn btn-default' href="{{route('estagio.pdf',[$estagio->id,'termo_compromisso'])}}" target="_blank">Visualizar</a>
                </div>
            @endif
            
            <div class="col-md-9">
                <div class="form-group">
                    <strong>Plano de Estágio:</strong>
                    {!! Form::file('plano_estagio', array('class' => 'form-control file')) !!}
                </div>
            </div>
            @if($estagio->plano_estagio == 'true')
                <div class="col-md-1">
                    &nbsp;
                    <button name="remover_documento" data-documento="plano_estagio" data-toggle="modal" data-target="#confirm-modal"  type="button" class="btn btn-danger">x</button>        
                </div>
                
                <div class="col-md-2">
                    &nbsp;
                    <a class='btn btn-default' href="{{route('estagio.pdf',[$estagio->id,'plano_estagio'])}}" target="_blank">Visualizar</a>
                </div>
            @endif
            
            <div class="col-md-9">
                <div class="form-group">
                    <strong>Controle de Frequência:</strong>
                    {!! Form::file('controle_frequencia', array('class' => 'form-control file')) !!}
                </div>
            </div>
            @if($estagio->controle_frequencia == 'true')
                <div class="col-md-1">
                    &nbsp;
                    <button name="remover_documento" data-documento="controle_frequencia" data-toggle="modal" data-target="#confirm-modal"  type="button" class="btn btn-danger">x</button>        
                </div>
                
                <div class="col-md-2">
                    &nbsp;
                    <a class='btn btn-default' href="{{route('estagio.pdf',[$estagio->id,'controle_frequencia'])}}" target="_blank">Visualizar</a>
                </div>
            @endif
            
            <div class="col-md-9">
                <div class="form-group">
                    <strong>Avaliação da Concedente:</strong>
                    {!! Form::file('relatorio_estagiario', array('class' => 'form-control file')) !!}
                </div>
            </div>
            @if($estagio->relatorio_estagiario == 'true')
                <div class="col-md-1">
                    &nbsp;
                    <button name="remover_documento" data-documento="relatorio_estagiario" data-toggle="modal" data-target="#confirm-modal"  type="button" class="btn btn-danger">x</button>        
                </div>
                
                <div class="col-md-2">
                    &nbsp;
                    <a class='btn btn-default' href="{{route('estagio.pdf',[$estagio->id,'relatorio_estagiario'])}}" target="_blank">Visualizar</a>
                </div>
            @endif
            
            <div class="col-md-9">
                <div class="form-group">
                    <strong>Avaliação do Estagiário:</strong>
                    {!! Form::file('relatorio_concedente', array('class' => 'form-control file')) !!}
                </div>
            </div>
            @if($estagio->relatorio_concedente == 'true')
                <div class="col-md-1">
                    &nbsp;
                    <button name="remover_documento" data-documento="relatorio_concedente" data-toggle="modal" data-target="#confirm-modal"  type="button" class="btn btn-danger">x</button>        
                </div>
                
                <div class="col-md-2">
                    &nbsp;
                    <a class='btn btn-default' href="{{route('estagio.pdf',[$estagio->id,'relatorio_concedente'])}}" target="_blank">Visualizar</a>
                </div>
            @endif
            
            <div class="col-md-9">
                <div class="form-group">
                    <strong>Relatório Final:</strong>
                    {!! Form::file('avaliacao_estagiario', array('class' => 'form-control file')) !!}
                </div>
            </div>
            @if($estagio->avaliacao_estagiario == 'true')
                <div class="col-md-1">
                    &nbsp;
                    <button name="remover_documento" data-documento="avaliacao_estagiario" data-toggle="modal" data-target="#confirm-modal" type="button" class="btn btn-danger">x</button>        
                </div>
                
                <div class="col-md-2">
                    &nbsp;
                    <a class='btn btn-default' href="{{route('estagio.pdf',[$estagio->id,'avaliacao_estagiario'])}}" target="_blank">Visualizar</a>
                </div>
            @endif
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-success" style="margin-bottom:15px"> ENVIAR </button>
        </div>

    </div>
    {!! Form::close() !!}
</div>


@endsection

@section('modal')
    <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content"></div>
        </div>
        <div class="modal-dialog">
            <div class="modal-content"></div>
        </div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div id="modal-participar" class="modal-header bg-danger">
                    <h4 class="modal-title" id="myModalLabel">Confirmação</h4>
                    <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span></button>

                </div>
                {{ Form::open(['method' => 'post', 'route' => ['estagio.destroyPdf', $estagio->id], 'class' => 'form-horizontal']) }}
                    <input id="documento" type="hidden" class="form-control" name="documento">
                    <div class="modal-body">
                        
                        Deseja excluir o documento informado?

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-danger">Excluir</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('select[name="aluno_id"]').select2();
        $('select[name="professor_id"]').select2();
        $('select[name="convenio_id"]').select2();
        
        $("button[name='remover_documento']").on('click', function() {
            var tipo_documento = $(this).data('documento'); 
            $("input[name='documento']").val(tipo_documento);
        });
    </script>
@endsection