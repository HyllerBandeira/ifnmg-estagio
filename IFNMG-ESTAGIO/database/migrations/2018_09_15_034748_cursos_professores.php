<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CursosProfessores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos_professores', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('professores_id');
            $table->unsignedInteger('cursos_id');
            $table->foreign('professores_id')->references('id')->on('professor')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('cursos_id')->references('id')->on('curso')->onUpdate('cascade');
            $table->string('access')->default('almenara')->notNull();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos_professores');
    }
}
