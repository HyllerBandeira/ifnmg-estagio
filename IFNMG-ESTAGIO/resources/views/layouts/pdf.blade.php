<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="css/components/bootstrap.min.css">
    <style>
    	.col {
    		float: left;
    	}/*
    	.col-1 {
    		width: 10%;
    	}
    	.col-2 {
    		width: 20%;
    	}
    	.col-4 {
    		width: 40%;
    	}
    	.col-6 {
    		width: 60%;
    	}
    	.col-8 {
    		width: 80%;
    	}
    	.col-1 {
    		width: 10%;
    	}
    	.col-10 {
    		width: 100%;
    	}*/

    	.container {
    		margin-left: 0px;
    		margin-right: 0px;
    		width: 100% !important;
    		max-width: none !important;
    		padding-left: .6604cm;
    		padding-right: .6604cm;
    	}
    	hr {
		    border: none;
		    height: 3px;
		    /* Set the hr color */
		    color: #333; /* old IE */
		    background-color: #333; /* Modern Browsers */
		}
	</style>
  </head>
  <body>
      <div class="container">
          @yield('content')
      </div>
  </body>
</html>