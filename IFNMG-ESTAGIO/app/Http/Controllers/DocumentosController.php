<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;

use Auth;
use App\Documentos;

class DocumentosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $access = Auth::user()->access ?? env('APP_ACCESS');
        
        $documentos = Documentos::where('access', $access)->orderBy('id','DESC')->get();
        return view('documento.index',compact('documentos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('documento.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required',
            'descricao' => 'required',
            'arquivo' => 'required',
            'arquivo_pdf' => 'required|mimes:pdf',
        ],[
            'required' => 'Este campo é obrigatório.',
            'mimes' => 'Formato do arquivo deve ser PDF'
        ]);
        
        // Adição do controle de acesso
        $request['access'] = Auth::user()->access;
        
        $documento = Documentos::where('nome',$request->nome)->first();
        if($documento) {
            $errorMessage[] = 'Nome já cadastrado.';
            
            return Redirect::back()->withInput(Input::all())->with('error', 'Nome já cadastrado.');
        }
        
        $documento = Documentos::create(
            [
                'nome' => $request->nome,
                'descricao' => $request->descricao,
                'arquivo' => $request->arquivo->getClientOriginalName(),
                'arquivo_pdf' => $request->arquivo_pdf->getClientOriginalName(),
                'access'=> $request['access']
            ]
        );
        
        if($request->file('arquivo') != null) {
            $path_file = $request->file('arquivo')->storeAs("documentos","{$documento->arquivo}");
        }
        
        if($request->file('arquivo_pdf') != null) {
            $path_file = $request->file('arquivo_pdf')->storeAs("documentos","{$documento->arquivo_pdf}");
        }

        return redirect()->route('documento.indexPublic')
                        ->with('success','Registro salvo com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($documento)
    {
        
        $file_path = "documentos/$documento";
        
        if(Storage::disk('local')->exists("/$file_path"))
            return response()->file(storage_path("app/$file_path"));
        
        return abort(404, "Documento não foi encontrado");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Documentos  $documento
     * @return \Illuminate\Http\Response
     */
    public function edit(Documentos $documento)
    {
        if (Gate::denies('documento.controlar', $documento)) {
            return abort(403, "Você não tem acesso a esta função");
        }
        
        return view('documento.edit',compact('documento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Documentos  $documento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Documentos $documento)
    {
        if (Gate::denies('documento.controlar', $documento)) {
            return abort(403, "Você não tem acesso a esta função");
        }
        
        $this->validate($request, [
            'nome' => 'required',
            'descricao' => 'required',
            'arquivo_pdf' => 'mimes:pdf',
        ],[
            'required' => 'Este campo é obrigatório.',
            'mimes' => 'Formato do arquivo deve ser PDF'
        ]);
        
        $error = Documentos::where('nome',$request->nome)
            ->where('id', '!=',  $documento->id)
            ->first();
        
        if($error) {
            $errorMessage[] = 'Nome já cadastrado.';
            
            return Redirect::back()->withInput(Input::all())->withErrors($errorMessage);
        }
        
        if($request->file('arquivo')) {
            $path_file = $request->file('arquivo')->storeAs("documentos",$request->arquivo->getClientOriginalName());
            $path_file_pdf = $request->file('arquivo_pdf')->storeAs("documentos",$request->arquivo_pdf->getClientOriginalName());
            $documento->update(
                [
                    'nome' => $request->nome,
                    'descricao' => $request->descricao,
                    'arquivo' => $request->arquivo->getClientOriginalName(),
                    'arquivo_pdf' => $request->arquivo_pdf->getClientOriginalName()
                ]
            );
        } else {
            $documento->update($request->all());
        }
        return redirect()->route('documento.indexPublic')
                        ->with('success','Registro alterado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Documentos  $documento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Documentos $documento)
    {
        if (Gate::denies('documento.controlar', $documento)) {
            return abort(403, "Você não tem acesso a esta função");
        }
        
        $documento->delete();
        return redirect()->route('documento.indexPublic')
                        ->with('success','Registro deletado com sucesso');
    }
}