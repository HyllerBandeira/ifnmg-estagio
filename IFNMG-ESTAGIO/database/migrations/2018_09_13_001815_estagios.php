<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Estagios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estagio', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('professor_id');
            $table->unsignedInteger('convenio_id');
            $table->unsignedInteger('aluno_id');
            $table->foreign('professor_id')->references('id')->on('professor')->onUpdate('cascade');
            $table->foreign('convenio_id')->references('id')->on('convenio')->onUpdate('cascade');
            $table->foreign('aluno_id')->references('id')->on('aluno')->onUpdate('cascade');
            $table->string('situacao')->nullable();
            $table->integer('carga_horaria')->nullable();
            $table->date('data_inicio')->nullable();
            $table->date('data_fim')->nullable();
            $table->enum('termo_compromisso' ,['true','false'])->nullable();
            $table->enum('plano_estagio' ,['true','false'])->nullable();
            $table->enum('relatorio_estagiario' ,['true','false'])->nullable();
            $table->enum('relatorio_concedente' ,['true','false'])->nullable();
            $table->enum('avaliacao_estagiario' ,['true','false'])->nullable();
            $table->enum('controle_frequencia' ,['true','false'])->nullable();
            $table->enum('carta_apresentacao' ,['true','false'])->nullable();
            $table->string('access')->default('almenara')->notNull();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estagio');
    }
}
