@extends('layouts.app')
 
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Editar Documento</h2>
                </div>
                <a href="{{ route('documento.indexPublic') }}">
                    <div class="btn btn-danger pull-right">
                        Voltar
                    </div>
                </a>
            </div>
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Opa!</strong> Tem algo de errado com as suas entradas.<br><br>
            </div>
        @endif

        {!! Form::model($documento, ['method' => 'PATCH', 'enctype' =>'multipart/form-data', 'route' => ['documento.update', $documento->id]]) !!}
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <label>Campos com <span class="mdi mdi-asterisk" style="color: red"></span> são obrigatórios</label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="nome">
                        <strong>Nome<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                    </label>
                    @if ($errors->has('nome'))
                        <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('nome') }}">
                            <img src="{{url('/img/alert-outline.png')}}"/>
                        </span>
                    @endif
                    {!! Form::text('nome', $documento->nome, array('placeholder' => 'Nome', 'class' => 'form-control')) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="descricao">
                        <strong>Descrição<span class="mdi mdi-asterisk" style="color: red"></span>: </strong>
                    </label>
                    @if ($errors->has('descricao'))
                        <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('descricao') }}">
                            <img src="{{url('/img/alert-outline.png')}}"/>
                        </span>
                    @endif
                    {!! Form::text('descricao', $documento->descricao, array('placeholder' => 'Descrição', 'class' => 'form-control')) !!}
                </div>
            </div>
            <div class="col-md-10">
                <div class="form-group">
                    <strong>Arquivo<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                    {!! Form::file('arquivo', array('class' => 'form-control file')) !!}
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <br>
                    <a class='btn btn-default' href="{{route('documento.show',$documento->arquivo)}}" target="_blank">Visualizar</a>
                </div>
            </div>
            
            <div class="col-md-10">
                <div class="form-group">
                    <strong>Arquivo PDF<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                    @if ($errors->has('arquivo_pdf'))
                        <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('arquivo_pdf') }}">
                            <img src="{{url('/img/alert-outline.png')}}"/>
                        </span>
                    @endif
                    {!! Form::file('arquivo_pdf', array('class' => 'form-control file')) !!}
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <br>
                    <a class='btn btn-default' href="{{route('documento.show',$documento->arquivo_pdf)}}" target="_blank">Visualizar</a>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-success"> ENVIAR </button>
            </div>
        </div>
    {!! Form::close() !!}
</div>
@endsection