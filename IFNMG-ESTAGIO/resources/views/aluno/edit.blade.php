@extends('layouts.app')
 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Aluno</h2>
            </div>
            <a href="{{ route('aluno.index') }}">
                <div class="btn btn-danger pull-right">
                    Voltar
                </div>
			</a>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Opa!</strong> Tem algo de errado com as suas entradas. Confira os campos<br><br>
        </div>
    @endif

    {!! Form::model($aluno, ['method' => 'PATCH', 'enctype' =>'multipart/form-data', 'route' => ['aluno.update', $aluno->id]]) !!}
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <label>Campos com <span class="mdi mdi-asterisk" style="color: red"></span> são obrigatórios</label>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="nome">
                    <strong>Nome<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('nome'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('nome') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('nome', $aluno->pessoa->nome, array('placeholder' => 'Nome', 'class' => 'form-control', 'maxlength' => 100)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-sm-9">
            <div class="form-group">
                <label for="cidade_natal">
                    <strong>Natural de<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('cidade_natal'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('cidade_natal') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('cidade_natal', $aluno->pessoa->cidade_natal, array('placeholder' => 'Cidade Natal', 'class' => 'form-control', 'maxlength' => 50)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-sm-1">
            <div class="form-group">
                <label for="uf_cidade_natal">
                    <strong>UF<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('uf_cidade_natal'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('uf_cidade_natal') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('uf_cidade_natal', $aluno->pessoa->uf_cidade_natal, array('placeholder' => 'UF', 'class' => 'form-control', 'maxlength' => 2)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-sm-2">
            <div class="form-group">
                <label for="nascimento">
                    <strong>Nascimento<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('nascimento'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('nascimento') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('nascimento', $aluno->pessoa->nascimento, array('placeholder' => 'Nascimento', 'class' => 'form-control date-picker', 'maxlength' => 8)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-sm-12">
            <div class="form-group">
                <label for="filiacao">
                    <strong>Filiação<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('filiacao'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('filiacao') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('filiacao', $aluno->pessoa->filiacao, array('placeholder' => 'Filiação', 'class' => 'form-control', 'maxlength' => 100)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-sm-3">
            <div class="form-group">
                <label for="estado_civil">
                    <strong>Estado Civil<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('estado_civil'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('estado_civil') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('estado_civil', $aluno->pessoa->estado_civil, array('placeholder' => 'Estado Civil', 'class' => 'form-control', 'maxlength' => 15)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-sm-2">
            <div class="form-group">
                <label for="sexo">
                    <strong>Sexo<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('sexo'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('sexo') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                <br>
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        {!! Form::radio('sexo', 'Masculino', ($aluno->pessoa->sexo == 'Masculino' ? true : false), array('class' => '')) !!}Masculino &nbsp;
                        {!! Form::radio('sexo', 'Feminino',($aluno->pessoa->sexo == 'Feminino' ? true : false), array('class' => '')) !!}Feminino
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-sm-3">
            <div class="form-group">
                <label for="rg">
                    <strong>RG<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('rg'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('rg') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('rg', $aluno->pessoa->rg, array('placeholder' => 'RG', 'class' => 'form-control', 'maxlength' => 15)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-sm-2">
            <div class="form-group">
                <label for="orgao_expeditor">
                    <strong>Orgão Expeditor<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('orgao_expeditor'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('orgao_expeditor') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('orgao_expeditor', $aluno->pessoa->orgao_expeditor, array('placeholder' => 'Orgão Expeditor', 'class' => 'form-control', 'maxlength' => 20)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-sm-2">
            <div class="form-group">
                <label for="cpf">
                    <strong>CPF<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                
                @if ($errors->has('cpf'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('cpf') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('cpf', $aluno->pessoa->cpf, array('placeholder' => 'CPF', 'class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-md-10">
            <div class="form-group">
                <strong>Foto:</strong>
                {!! Form::file('foto', array('class' => 'form-control file')) !!}
            </div>
        </div>

        <div class="col-md-2">
            <div class="row">&nbsp;</div>
            <a class='btn btn-default' href="{{route('aluno.foto',[$aluno->id,$aluno->foto])}}" target="_blank">Visualizar Foto</a>
        </div>

        <div class="col-xs-12 col-sm-10">
            <div class="form-group">
                <label for="endereco">
                    <strong>Endereço<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('endereco'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('endereco') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('endereco', $aluno->pessoa->endereco, array('placeholder' => 'Endereço', 'class' => 'form-control', 'maxlength' => 50)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label for="numero">
                    <strong>Nº<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('numero'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('numero') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('numero', $aluno->pessoa->numero, array('placeholder' => 'Nº', 'class' => 'form-control', 'maxlength' => 10)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="complemento">
                    <strong>Complemento:</strong>
                </label>
                @if ($errors->has('complemento'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('complemento') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('complemento', $aluno->pessoa->complemento, array('placeholder' => 'Complemento', 'class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label for="bairro">
                    <strong>Bairro<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('bairro'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('bairro') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('bairro', $aluno->pessoa->bairro, array('placeholder' => 'Bairro','class' => 'form-control', 'maxlength' => 50)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-5">
            <div class="form-group">
                <label for="cidade">
                    <strong>Cidade<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('cidade'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('cidade') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('cidade', $aluno->pessoa->cidade, array('placeholder' => 'Cidade','class' => 'form-control', 'maxlength' => 50)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-1">
            <div class="form-group">
                <label for="uf">
                    <strong>UF<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('uf'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('uf') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('uf', $aluno->pessoa->uf, array('placeholder' => 'UF', 'class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label for="cep">
                    <strong>CEP<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('cep'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('cep') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('cep', $aluno->pessoa->cep, array('placeholder' => 'CEP', 'class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label for="celular">
                    <strong>Celular<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('celular'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('celular') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('celular', $aluno->pessoa->celular, array('placeholder' => 'Celular', 'class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label for="telefone">
                    <strong>Telefone:</strong>
                </label>
                @if ($errors->has('telefone'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('telefone') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('telefone', $aluno->pessoa->telefone, array('placeholder' => 'Telefone', 'class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="email">
                    <strong>Email<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('email'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('email') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('email', $aluno->pessoa->email, array('placeholder' => 'Email', 'class' => 'form-control', 'maxlength' => 100)) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="matricula">
                    <strong>Nº Matricula<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('matricula'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('matricula') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('matricula', $aluno->matricula, array('placeholder' => 'Nº Matricula', 'class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="turma">
                    <strong>Turma<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                </label>
                @if ($errors->has('turma'))
                    <span class="alerta" tabindex="0" data-toggle="tooltip" data-placement="top" title="{{ $errors->first('turma') }}">
                        <img src="{{url('/img/alert-outline.png')}}"/>
                    </span>
                @endif
                {!! Form::text('turma', $aluno->turma, array('placeholder' => 'Código da turma', 'class' => 'form-control')) !!}
            </div>
        </div>
        
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Curso<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
				{!! Form::select('curso_id', $cursos, $aluno->curso_id, array('class' => 'form-control')) !!}
			</div>
		</div>
            
        <div class="col-md-10">
            <div class="form-group">
                <strong>Ficha do Matricula<span class="mdi mdi-asterisk" style="color: red"></span>:</strong>
                {!! Form::file('ficha_matricula', array('class' => 'form-control file')) !!}
            </div>
        </div>

        <div class="col-md-2">
            <div class="row">&nbsp;</div>
            <a class='btn btn-default' href="{{route('aluno.pdf',[$aluno->id,'ficha_matricula'])}}" target="_blank">Visualizar</a>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-success" style="margin-bottom:15px"> ENVIAR </button>
            </div>
        </div>

    </div>
    {!! Form::close() !!}
</div>

@endsection

@section('script')
    <script>
        $('input[name="nascimento"]').datepicker({
            calendarWeeks: false,
            language: "pt-BR",
            format: "dd/mm/yyyy"
        });
        $('input[name="telefone"]').mask('(00) 0000-0000');
        $('input[name="celular"]').mask('(00) 00000-0000');
        $('input[name="cep"]').mask('00000-000');
        $('input[name="uf"]').mask('SS');
        $('input[name="uf_cidade_natal"]').mask('SS');
        $('input[name="orgao_expeditor"]').mask('SSSSS');
        $('input[name="cpf"]').mask('000.000.000-00');
        
        $('select[name="curso_id"]').select2();
        
        
        $("button[name='remover_documento']").on('click', function() {
            var tipo_documento = $(this).data('documento'); 
            $("input[name='documento']").val(tipo_documento);
        });
    </script>
@endsection