@extends('layouts.default')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Adicionar nova Pessoa</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('pessoa.index') }}"> Voltar</a>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Opa!</strong> Tem algo de errado com as suas entradas.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open(array('route' => 'pessoa.store','method'=>'POST')) !!}
    <div class="row">
        
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Tipo:</strong>
				{!! Form::select('tipo', array('aluno'=>'Aluno', 'professor'=>'Professor'), null, array('class' => 'form-control')) !!}
			</div>
		</div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nome:</strong>
                {!! Form::text('nome', null, array('placeholder' => 'Nome','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>RG:</strong>
                {!! Form::text('rg', null, array('placeholder' => 'RG','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>CPF:</strong>
                {!! Form::text('cpf', null, array('placeholder' => 'CPF','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nascimento:</strong>
                {!! Form::text('nascimento', null, array('placeholder' => 'Nascimento','class' => 'form-control date-picker')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Logradouro:</strong>
                {!! Form::text('endereco', null, array('placeholder' => 'Logradouro','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nº:</strong>
                {!! Form::text('numero', null, array('placeholder' => 'Nº','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Complemento:</strong>
                {!! Form::text('complemento', null, array('placeholder' => 'Complemento','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Bairro:</strong>
                {!! Form::text('bairro', null, array('placeholder' => 'Bairro','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Cidade:</strong>
                {!! Form::text('cidade', null, array('placeholder' => 'Cidade','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>UF:</strong>
                {!! Form::text('uf', null, array('placeholder' => 'UF','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>CEP:</strong>
                {!! Form::text('cep', null, array('placeholder' => 'CEP','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Celular:</strong>
                {!! Form::text('celular', null, array('placeholder' => 'Celular','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Telefone:</strong>
                {!! Form::text('telefone', null, array('placeholder' => 'Telefone','class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submeter</button>
        </div>

    </div>
    {!! Form::close() !!}

@endsection

@section('script')
    <script>
        $('input[name="nascimento"]').datepicker({
            calendarWeeks: false,
            language: "pt-BR",
            format: "dd/mm/yyyy"
        });
        $('input[name="telefone"]').mask('(00) 0000-0000');
        $('input[name="celular"]').mask('(00) 00000-0000');
        $('input[name="cep"]').mask('00000-000');
        $('input[name="uf"]').mask('SS');
        $('input[name="cpf"]').mask('000.000.000-00');
    </script>
@endsection