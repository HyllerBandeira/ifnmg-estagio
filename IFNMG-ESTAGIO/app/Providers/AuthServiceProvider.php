<?php

namespace App\Providers;

use App\Alunos;
use App\Pessoas;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
        Gate::define('aluno.controlar', function ($user, $aluno) {
            return ($user->access == $aluno->access);
        });
        
        Gate::define('area.controlar', function ($user, $area) {
            return ($user->access == $area->access);
        });
        
        Gate::define('convenio.controlar', function ($user, $convenio) {
            return ($user->access == $convenio->access);
        });
        
        Gate::define('curso.controlar', function ($user, $curso) {
            return ($user->access == $curso->access);
        });
        
        Gate::define('documento.controlar', function ($user, $documento) {
            return ($user->access == $documento->access);
        });
        
        Gate::define('estagio.controlar', function ($user, $estagio) {
            return ($user->access == $estagio->access);
        });
        
        Gate::define('pessoa.controlar', function ($user, $pessoa) {
            return ($user->access == $pessoa->access);
        });
        
        Gate::define('professor.controlar', function ($user, $professor) {
            return ($user->access == $professor->access);
        });
        
        Gate::define('professor.controlar', function ($user, $professor) {
            return ($user->access == $professor->access);
        });
        
        Gate::define('user.verMeuEstagio', function ($user) {
            $contador = Alunos::whereHas('pessoa', function($query) use($user) {
                $query->where([
                    ['nome', $user->name],
                    ['email', $user->email],
                    ['access', $user->access],
                ]);
            })->get()->count();
            
            return ($contador);
        });
        
        Gate::define('user.admin', function ($user) {
            return ($user->auth == 'coordenador' || $user->auth == 'estagiario');
        });
        
        Gate::define('user.superadmin', function ($user) {
            return ($user->auth == 'coordenador');
        });
    }
}
