@extends('layouts.app')
 
@section('content')
<?php
use Illuminate\Support\Facades\Input;?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-9">Listagem de Alunos</div>
                            @can('user.admin')
                                <div class="text-right col-md-3"><a class="btn btn-default btn-sm" href="{{ route('aluno.create') }}" role="button">Novo Aluno</a></div>
                            @endcan
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-ordering" >
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>CPF</th>
                                        <th>N° Matrícula</th>
                                        <th width="150px" >Horas cumpridas</th>
                                        <th width="200px">Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($alunos as $key => $aluno)
                                    <tr>
                                        <td>{{ $aluno->pessoa->nome }}</td>
                                        <td>{{ $aluno->pessoa->cpf }}</td>
                                        <td class="text-right">{{ $aluno->matricula }}</td>
                                        <td class="text-right">{{ $aluno->carga_horaria }}</td>
                                        <td>
                                            <a class="btn btn-default btn-sm" href="{{ route('aluno.show',$aluno->id) }}">Abrir</a>
                                            <a class="btn btn-primary btn-sm" href="{{ route('aluno.edit',$aluno->id) }}">Editar</a>
                                            {!! Form::open(['method' => 'DELETE','route' => ['aluno.destroy', $aluno->id],'style'=>'display:inline','class'=>'form-delete-cofirm', 'data-confirmed'=>'false']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection