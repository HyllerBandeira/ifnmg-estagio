<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pessoas;
use App\Cursos;
use App\Estagios;

class Alunos extends Model
{
    protected $table = 'aluno';
    public $fillable = [
        'id',
        'pessoa_id',
        'curso_id',
        'matricula',
        'turma',
        'foto',
        'situacao_estagio',
        'ficha_matricula',
        'access',
    ];
    protected $hidden = ['access'];
    
    public function curso() {
        return $this->belongsTo('App\Cursos');
    }
    
    public function pessoa() {
        return $this->belongsTo('App\Pessoas');
    }
    public function estagios()
    {
        return $this->hasMany('App\Estagios', 'aluno_id');
    }
    public function getCargaHorariaAttribute() {
        return $this->estagios->sum('carga_horaria');
    }
    public function getNomeAttribute() {
        return $this->pessoa->nome;
    }
    
    public function checkStatus() {
        $situacao_estagio = 'Em Andamento';
        
        $carga_horaria = $this->carga_horaria;
        $estagios_em_andamento = $this->estagios()->where('situacao','!=','Concluído')->get();
        
        if($carga_horaria >= (int)($this->curso->carga_horaria) && ($estagios_em_andamento->count() == 0)) {
            $situacao_estagio = 'Finalizado';
        }
        
        if($this->situacao_estagio != $situacao_estagio) {
            $this->update(['situacao_estagio' => $situacao_estagio]);
        }
    }
}
