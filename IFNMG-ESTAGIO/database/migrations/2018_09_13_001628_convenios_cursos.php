<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConveniosCursos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convenios_cursos', function (Blueprint $table) {
            $table->unsignedInteger('convenios_id');
            $table->unsignedInteger('cursos_id');
            $table->foreign('convenios_id')->references('id')->on('convenio')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('cursos_id')->references('id')->on('curso')->onUpdate('cascade');
            $table->string('access')->default('almenara')->notNull();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
