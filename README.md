# README #

Abre o arquivo .env (Se encontra no diretório principal)

Informe os valores para as constantes: DB_HOST, DB_PORT, DB_USERNAME, DB_PASSWORD

Estas informações deverão ser pegadas com o Técnico que estiver upando o sistema.

Tem um SQL na pasta database, com o nome ifnmg-estagio.sql, ele contém todo o banco de dados do sistema, execute ele no BD.

Caso necessário, configure o vHost em seu servidor.

Por fim atualize o composer e gere a chave de acesso.