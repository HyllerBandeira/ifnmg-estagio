<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Professores extends Model
{
    use Notifiable;
    
    protected $table = 'professor';
    public $fillable = ['id','nome','email','access',];
    protected $hidden = ['access'];
    
    public function cursos() {
        return $this->belongsToMany('App\Cursos');
    }
}
