@extends('layouts.app')
 
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">

                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-9">Listagem de Estágios</div>
                            @can('user.admin')
                                <div class="text-right col-md-3"><a class="btn btn-default btn-sm" href="{{ route('estagio.create') }}" role="button">Novo Estágio</a></div>
                            @endcan
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-ordering table-estagios" table-paging="false">
                                <thead>
                                    <tr>
                                        <th>Aluno</th>
                                        <th>Situação</th>
                                        <th>Carga Hr.</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($alunos as $aluno)
                                        <tr class="tr-aluno" data-aluno-id="{{ $aluno->id }}">
                                            <td><strong>{{ $aluno->pessoa->nome }}</strong></td>
                                            <td><strong>{{ $aluno->situacao_estagio }}</strong></td>
                                            <td class="text-right"><strong>{{ $aluno->carga_horaria }}</strong></td>
                                            <td>
                                                <button name="aluno-modal" data-toggle="modal" data-target="#aluno-{{$aluno->id}}-modal" type="button" class="btn btn-default">Expandir</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')

    @foreach ($alunos as $aluno)
        <div class="modal fade bd-example-modal-lg" id="aluno-{{$aluno->id}}-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h4 class="modal-title">{{$aluno->nome}}</h4>
                        <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Fechar</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped " table-paging="false">
                                <thead>
                                    <tr>
                                        <th>Aluno</th>
                                        <th>Local</th>
                                        <th>Convênio</th>
                                        <th>Situação</th>
                                        <th>Carga Hr.</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($aluno->estagios as $key => $estagio)
                                        <tr>
                                            @php  $convenio = $estagio->convenio; @endphp
                                            <td>{{ $estagio->aluno->pessoa->nome }}</td>
                                            <td>{{ $convenio->tipo == 'fisica' ? $convenio->nome : $convenio->nome_fantasia }}</td>
                                            <td>{{ $estagio->professor->nome }}</td>
                                            <td>{{ $estagio->situacao }}</td>
                                            <td class="text-right">{{ $estagio->carga_horaria }}</td>
                                            <td>
                                                <a class="btn btn-default btn-sm" href="{{ route('estagio.show',$estagio->id) }}">Abrir</a>
                                                <a class="btn btn-primary btn-sm" href="{{ route('estagio.edit',$estagio->id) }}">Editar</a>
                                                {!! Form::open(['method' => 'DELETE','route' => ['estagio.destroy', $estagio->id], 'style'=>'display:inline', 'class'=>'form-delete-cofirm', 'data-confirmed'=>'false']) !!}
                                                {!! Form::submit('Excluir', ['class' => 'btn btn-danger btn-sm']) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        .
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@section('script')
    <script>
    </script>
@endsection