<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

use App\Pessoas;
use Auth;

class PessoaController extends Controller
{
    public function index(Request $request)
    {
        $access = Auth::user()->access ?? env('APP_ACCESS');
        
        $pessoas = Pessoas::where('access', $access)->orderBy('id','DESC')->get();
        return view('pessoa.index',compact('pessoas'));
    }
    
    public function create(Request $request) {
        return view('pessoa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validação dos campos necessários
        $this->validate($request, [
            'nome' => 'required',
            'rg' => 'required',
            'cpf' => 'required',
            'nascimento' => 'required',
            'endereco' => 'required',
            'numero' => 'required',
            'bairro' => 'required',
            'cidade' => 'required',
            'uf' => 'required',
            'cep' => 'required',
        ]);
        
        // Formatação dos campos necessários
        $request['nascimento'] = implode('-',array_reverse(explode('/',$request->nascimento)));
        
        // Adição do controle de acesso
        $request['access'] = Auth::user()->access;

        Pessoas::create($request->all());
        return redirect()->route('pessoa.index')
                        ->with('success','Registro salvo com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pessoa = Pessoas::findOrFail($id);
        
        // Formatação dos atributos necessários
        $pessoa['nascimento'] = implode('/',array_reverse(explode('-',$pessoa->nascimento)));
        
        return view('pessoa.show',compact('pessoa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pessoa = Pessoas::findOrFail($id);
        
        // Formatação dos atributos necessários
        $pessoa['nascimento'] = implode('/',array_reverse(explode('-',$pessoa->nascimento)));
        
        return view('pessoa.edit',compact('pessoa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome' => 'required',
            'rg' => 'required',
            'cpf' => 'required',
            'nascimento' => 'required',
            'endereco' => 'required',
            'numero' => 'required',
            'bairro' => 'required',
            'cidade' => 'required',
            'uf' => 'required',
            'cep' => 'required',
            'telefone' => 'required'
        ]);

        // Formatação dos campos necessários
        $request['nascimento'] = implode('-',array_reverse(explode('/',$request->nascimento)));
        
        Pessoas::findOrFail($id)->update($request->all());
        return redirect()->route('pessoa.index')
                        ->with('success','Registro alterado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pessoas::findOrFail($id)->delete();
        return redirect()->route('pessoa.index')
                        ->with('success','Registro deletado com sucesso');
    }
}