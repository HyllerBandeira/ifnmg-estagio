@extends('layouts.default')
 
@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Gerenciamento de Area de Atuação</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('areaAtuacao.create') }}"> Adicionar Nova Area de Atuação</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered table-ordering"  style="width:100%">
        <tr>
            <th>Nome</th>
            <th>Descrição</th>
            <th width="200px">Ação</th>
        </tr>
    @foreach ($areas as $key => $area)
    <tr>
        <td>{{ $area->nome }}</td>
        <td>{{ $area->descricao }}</td>
        <td>
            <a class="btn btn-info" href="{{ route('areaAtuacao.show',$area->id) }}">Abrir</a>
            <a class="btn btn-primary" href="{{ route('areaAtuacao.edit',$area->id) }}">Editar</a>
            {!! Form::open(['method' => 'DELETE','route' => ['areaAtuacao.destroy', $area->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>

@endsection