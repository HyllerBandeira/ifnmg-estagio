<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return view('welcome');
})->name('/');

Route::group(['middleware' => ['can:user.admin']], function() {
    Route::post('/aluno','AlunoController@index')->name('aluno.filtro');
    Route::resource('aluno','AlunoController');
    Route::get('/aluno/{aluno_id}/pdf/{documento}','AlunoController@showPdf')->name('aluno.pdf');
    Route::get('/aluno/{aluno_id}/fotos/{foto}','AlunoController@showFoto')->name('aluno.foto');
    Route::post('/aluno/{aluno_id}/destroyPdf','AlunoController@destroyPdf')->name('aluno.destroyPdf');

    Route::resource('areaAtuacao','AreaAtuacaoController');

    Route::resource('convenio','ConveniosController');
    Route::get('/convenio/{convenio_id}/pdf/cadastro_convenio','ConveniosController@showPdf')->name('convenio.pdf');
    Route::post('/convenio/{convenio_id}/destroyPdf','ConveniosController@destroyPdf')->name('convenio.destroyPdf');

    Route::resource('curso','CursoController');
    Route::resource('estagio','EstagiosController');
    Route::get('/estagio/create/{aluno_id}','EstagiosController@create')->name('estagio.create()');
    Route::get('/estagio/{estagio_id}/edit/{aluno_id}','EstagiosController@edit')->name('estagio.edit()');
    Route::get('/estagio/{estagio_id}/pdf/{documento}','EstagiosController@showPdf')->name('estagio.pdf');
    Route::post('/estagio/{estagio_id}/destroyPdf','EstagiosController@destroyPdf')->name('estagio.destroyPdf');
    Route::delete('/estagio/{estagio_id}/destroy/{aluno_id}','EstagiosController@destroy')->name('estagio.destroy()');

    Route::resource('pessoa','PessoaController');

    Route::resource('professor','ProfessoresController');
    Route::resource('documento','DocumentosController');

});
Route::group(['middleware' => ['can:user.verMeuEstagio']], function() {
    Route::get('/meusEstagios','AlunoController@meusEstágios')->name('aluno.meusEstagios');
    Route::get('/meusEstagios/{aluno_id}','AlunoController@meusEstágios')->name('aluno.meuEstagio');
    Route::get('/estagio/{estagio_id}/pdf/{documento}','EstagiosController@showPdf')->name('estagio.pdf');
});

Route::get('/convenio','ConveniosController@index')->name('convenio.indexPublic');
Route::get('/documento','DocumentosController@index')->name('documento.indexPublic');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/convenio/public/{convenio}','ConveniosController@show')->name('convenio.abrir');
Route::get('/documento/public/{documentos}','DocumentosController@show')->name('documento.abrir');
Route::get('/convenio/{convenio_id}/pdf/cadastro_convenio','ConveniosController@showPdf')->name('convenio.pdf');
