@extends('layouts.app')
 
@section('content')
<?php
use Illuminate\Support\Facades\Input;?>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-9">Listagem de Alunos</div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-ordering" >
                                <thead>
                                    <tr>
                                        <th>Curso</th>
                                        <th width="150px" >Horas cumpridas</th>
                                        <th width="50px" >Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($alunos as $key => $aluno)
                                    <tr>
                                        <td>{{ $aluno->curso->nome }}</td>
                                        <td class="text-right">{{ $aluno->carga_horaria }}</td>
                                        <td>
                                            <a class="btn btn-default btn-sm" href="{{ route('aluno.meuEstagio',$aluno->id) }}">Abrir</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection