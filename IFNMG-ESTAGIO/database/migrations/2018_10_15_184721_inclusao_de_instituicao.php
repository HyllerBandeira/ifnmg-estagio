<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InclusaoDeInstituicao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Adiciona auth na tabela de uruários, campo indicará a autorização do usuario
        if(!Schema::hasColumn('users', 'auth')) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('auth')->after('password')->default('aluno')->notNull();
            });
        }
        
        //Adiciona access na tabela de uruários, campo indicará o acesso do usuario, no caso a instituição que ele acessa
        if(!Schema::hasColumn('users', 'access')) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('access')->after('auth')->default('almenara')->notNull();
            });
        }
        
        //Adiciona access na tabela de area, campo indicará a qual instituição a area pertence
        if(!Schema::hasColumn('area', 'access')) {
            Schema::table('area', function (Blueprint $table) {
                $table->string('access')->notNull();
            });
        }
        
        //Adiciona access na tabela de area, campo indicará a qual instituição o aluno pertence
        if(!Schema::hasColumn('aluno', 'access')) {
            Schema::table('aluno', function (Blueprint $table) {
                $table->string('access')->notNull();
            });
        }
        
        //Adiciona access na tabela de area, campo indicará a qual instituição o convênio pertence
        if(!Schema::hasColumn('convenio', 'access')) {
            Schema::table('convenio', function (Blueprint $table) {
                $table->string('access')->notNull();
            });
        }
        
        //Adiciona access na tabela de area, campo indicará a qual instituição o curso pertence
        if(!Schema::hasColumn('curso', 'access')) {
            Schema::table('curso', function (Blueprint $table) {
                $table->string('access')->notNull();
            });
        }
        
        //Adiciona access na tabela de area, campo indicará a qual instituição o documento pertence
        if(!Schema::hasColumn('documento', 'access')) {
            Schema::table('documento', function (Blueprint $table) {
                $table->string('access')->notNull();
            });
        }
        
        //Adiciona access na tabela de area, campo indicará a qual instituição o estágio pertence
        if(!Schema::hasColumn('estagio', 'access')) {
            Schema::table('estagio', function (Blueprint $table) {
                $table->string('access')->notNull();
            });
        }
        
        //Adiciona access na tabela de area, campo indicará a qual instituição a pessoa pertence
        if(!Schema::hasColumn('pessoa', 'access')) {
            Schema::table('pessoa', function (Blueprint $table) {
                $table->string('access')->notNull();
            });
        }
        
        //Adiciona access na tabela de area, campo indicará a qual instituição o professor pertence
        if(!Schema::hasColumn('professor', 'access')) {
            Schema::table('professor', function (Blueprint $table) {
                $table->string('access')->notNull();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('users', 'auth')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('auth');
            });
        }
        if(Schema::hasColumn('users', 'access')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('access');
            });
        }
        if(Schema::hasColumn('area', 'access')) {
            Schema::table('area', function (Blueprint $table) {
                $table->dropColumn('access');
            });
        }
        if(Schema::hasColumn('aluno', 'access')) {
            Schema::table('aluno', function (Blueprint $table) {
                $table->dropColumn('access');
            });
        }
        if(Schema::hasColumn('convenio', 'access')) {
            Schema::table('convenio', function (Blueprint $table) {
                $table->dropColumn('access');
            });
        }
        if(Schema::hasColumn('curso', 'access')) {
            Schema::table('curso', function (Blueprint $table) {
                $table->dropColumn('access');
            });
        }
        if(Schema::hasColumn('documento', 'access')) {
            Schema::table('documento', function (Blueprint $table) {
                $table->dropColumn('access');
            });
        }
        if(Schema::hasColumn('estagio', 'access')) {
            Schema::table('estagio', function (Blueprint $table) {
                $table->dropColumn('access');
            });
        }
        if(Schema::hasColumn('pessoa', 'access')) {
            Schema::table('pessoa', function (Blueprint $table) {
                $table->dropColumn('access');
            });
        }
        if(Schema::hasColumn('professor', 'access')) {
            Schema::table('professor', function (Blueprint $table) {
                $table->dropColumn('access');
            });
        }
    }
}
