<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Convenios extends Model
{
    protected $table = 'convenio';
    public $fillable = [
		'id',
		'tipo',
		'nome',
		'nome_fantasia', 
		'ramo_atividade',
		'cpf_cnpj',
		'endereco',
		'numero',
		'complemento',
		'bairro',
		'cidade',
		'estado',
		'cep',
		'telefone',
		'fax',
		'email',
		'representante_legal',
		'cargo_habilitacao',
		'conselho_classe',
		'numero_registro',
		'cadastro_convenio',
        'access',
		];
    protected $hidden = ['access'];
    
    public function cursos() {
        return $this->belongsToMany('App\Cursos');
    }
}

		