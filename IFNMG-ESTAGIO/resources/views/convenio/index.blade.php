@extends('layouts.app')
 
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-9">Listagem de Convênios</div>
                            @can('user.admin')
                                <div class="text-right col-md-3"><a class="btn btn-default btn-sm" href="{{ route('convenio.create') }}" role="button">Novo Convênio</a></div>
                            @endcan
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-ordering" >
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Ramo de Atividade</th>
                                        <th>Cidade</th>
                                        <th >CPF/CNPJ</th>
                                        <th width="200px">Ação</th>
                                        <th class="hide">Nome</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($convenios as $key => $convenio)
                                        <tr>
                                            <td>{{ $convenio->tipo == 'fisica' ? $convenio->nome : $convenio->nome_fantasia }}</td>
                                            <td>{{ $convenio->ramo_atividade }}</td>
                                            <td>{{ $convenio->cidade }}</td>
                                            <td>{{ $convenio->cpf_cnpj }}</td>
                                            <td>
                                                <a class="btn btn-default btn-sm" href="{{ route('convenio.abrir',$convenio->id) }}">Abrir</a>
                                                @can('user.admin')
                                                    <a class="btn btn-primary btn-sm" href="{{ route('convenio.edit',$convenio->id) }}">Editar</a>
                                                    {!! Form::open(['method' => 'DELETE','route' => ['convenio.destroy', $convenio->id],'style'=>'display:inline','class'=>'form-delete-cofirm', 'data-confirmed'=>'false']) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                                    {!! Form::close() !!}
                                                @endcan
                                            </td>
                                            <td class="hide">{{ $convenio->nome}}</td>
                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection