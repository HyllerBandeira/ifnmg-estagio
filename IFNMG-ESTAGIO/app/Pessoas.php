<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoas extends Model
{
    protected $table = 'pessoa';
    public $fillable = [
        'id',
        'tipo',
        'nome',
        'cidade_natal',
        'uf_cidade_natal',
        'nascimento',
        'filiacao',
        'estado_civil',
        'sexo',
        'rg',
        'orgao_expeditor',
        'cpf',
        'endereco',
        'numero',
        'complemento',
        'bairro',
        'cidade',
        'uf',
        'cep',
        'celular',
        'telefone',
        'email',
        'access',
    ];
    protected $hidden = ['access'];

    public function aluno()
    {
        return $this->hasOne(Alunos::class, 'pessoa_id');
    }
}
