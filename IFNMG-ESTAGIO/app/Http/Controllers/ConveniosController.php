<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;

use Auth;
use App\Convenios;
use App\Cursos;

class ConveniosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $access = Auth::user()->access ?? env('APP_ACCESS');
        
        $convenios = Convenios::where('access', $access)->orderBy('id','DESC')->get();
        
        return view('convenio.index',compact('convenios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $retorno = Cursos::orderBy('id','DESC')->get();
        $cursos;
        foreach($retorno as $curso) {
            $cursos[$curso->id] = $curso->nome;
        }
        $error = 0;
        $errorMessage = [];
        if(empty($cursos)) {
            $cursos = [];
            $errorMessage[] = 'Não existem cursos cadastrados;';
            $error = true;
        }
        
        if($error)
            return view('convenio.create', compact('cursos'))->withErrors($errorMessage);
        
        return view('convenio.create', compact('cursos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		if($request->tipo == 'fisica')
			$this->validate($request, [
				'tipo' => 'required',
				'nome' => 'required',
				'ramo_atividade' => 'required',
				'cpf_cnpj' => 'required',
				'endereco' => 'required',
				'numero' => 'required', 
				'bairro' => 'required',
				'cidade' => 'required',
				'estado' => 'required',
				'cep' => 'required',
				'cargo_habilitacao' => 'required',
				'cadastro_convenio' => 'required',
			]);
		else
			$this->validate($request, [
				'tipo' => 'required',
				'nome' => 'required',
				'nome_fantasia' => 'required',
				'ramo_atividade' => 'required',
				'cpf_cnpj' => 'required',
				'endereco' => 'required',
				'numero' => 'required', 
				'bairro' => 'required',
				'cidade' => 'required',
				'estado' => 'required',
				'cep' => 'required',
				'representante_legal' => 'required',
				'cargo_habilitacao' => 'required',
				'cadastro_convenio' => 'required|mimes:pdf',
			],[
                'required' => 'Este campo é obrigatório.',
                'integer' => 'Apenas numeros.',
                'mimes' => 'Formato do arquivo deve ser PDF'
            ]);
        
        // Adição do controle de acesso
        $request['access'] = Auth::user()->access;
        
        $convenio_data = $request->all();
        $convenio_data['cadastro_convenio'] = 'false';
        
        $convenio = Convenios::create($convenio_data);
        
        // UPLOAD DOS ARQUIVOS
        if($request->file('cadastro_convenio') != null) {
            $path_controle_frequencia = $request->file('cadastro_convenio')
                                            ->storeAs("convenio/$convenio->id/pdfs","cadastro_convenio.pdf");
            $convenio_data['cadastro_convenio'] = 'true';
        }
        $convenio->update($convenio_data);
        
        $convenio->cursos()->sync($request->input('curso_id'));
        
        return redirect()->route('convenio.indexPublic')
                        ->with('success','Registro salvo com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  Convenios  $convenio
     * @return \Illuminate\Http\Response
     */
    public function show(Convenios $convenio)
    {
        return view('convenio.show',compact('convenio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Convenios  $convenio
     * @return \Illuminate\Http\Response
     */
    public function edit(Convenios $convenio)
    {
        if (Gate::denies('convenio.controlar', $convenio)) {
            return abort(403, "Você não tem acesso a esta função");
        }
        
        $retorno = Cursos::orderBy('id','DESC')->get();
        $cursos;
        foreach($retorno as $curso) {
            $cursos[$curso->id] = $curso->nome;
        }
        
        $i = 0;
        foreach($convenio->cursos()->get() as $curso) {
            $convenio->cursos .= ($i==0)? $curso->id: ','.$curso->id;
            $i++;
        }
        $error = 0;
        $errorMessage = [];
        if(empty($cursos)) {
            $cursos = [];
            $errorMessage[] = 'Não existem cursos cadastrados;';
            $error = true;
        }
        
        if($error)
            return view('convenio.edit',['convenio' => $convenio, 'cursos' => $cursos])->withErrors($errorMessage);
        
        return view('convenio.edit',['convenio' => $convenio, 'cursos' => $cursos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Convenios  $convenio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Convenios $convenio)
    {
        if (Gate::denies('convenio.controlar', $convenio)) {
            return abort(403, "Você não tem acesso a esta função");
        }
        
		if($request->tipo == 'fisica')
			$this->validate($request, [
				'tipo' => 'required',
				'nome' => 'required',
				'ramo_atividade' => 'required',
				'cpf_cnpj' => 'required',
				'endereco' => 'required',
				'numero' => 'required', 
				'bairro' => 'required',
				'cidade' => 'required',
				'estado' => 'required',
				'cep' => 'required',
				'cargo_habilitacao' => 'required',
			]);
		else
			$this->validate($request, [
				'tipo' => 'required',
				'nome' => 'required',
				'nome_fantasia' => 'required',
				'ramo_atividade' => 'required',
				'cpf_cnpj' => 'required',
				'endereco' => 'required',
				'numero' => 'required', 
				'bairro' => 'required',
				'cidade' => 'required',
				'estado' => 'required',
				'cep' => 'required',
				'representante_legal' => 'required',
				'cargo_habilitacao' => 'required',
				'cadastro_convenio' => 'mimes:pdf',
			],[
                'required' => 'Este campo é obrigatório.',
                'integer' => 'Apenas numeros.',
                'mimes' => 'Formato do arquivo deve ser PDF'
            ]);
        $convenio_data = $request->all();
        
        // UPLOAD DOS ARQUIVOS
        if($request->file('cadastro_convenio')) {
            $path_controle_frequencia = $request->file('cadastro_convenio')
                                            ->storeAs("convenio/$convenio->id/pdfs","cadastro_convenio.pdf");
            $convenio_data['cadastro_convenio'] = 'true';
        }
        $convenio->update($convenio_data);
        
        return redirect()->route('convenio.indexPublic')
                        ->with('success','Registro alterado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Convenios  $convenio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Convenios $convenio)
    {
        if (Gate::denies('convenio.controlar', $convenio)) {
            return abort(403, "Você não tem acesso a esta função");
        }
        
        $convenio->cursos()->detach();
         try{
            
           $convenio->delete();;
            
        } catch (\PDOException $e) {
            
            return redirect()->route('convenio.indexPublic')->with('error', 'Não é possível excluir o convênio, pois há estágios cadastrados para este convênio.');
        }
        
        return redirect()->route('convenio.indexPublic')
                        ->with('success','Registro deletado com sucesso');
    }
    
    /**
     * Get the specified document from storage.
     *
     * @param  Convenios  $convenio
     * @return \Illuminate\Http\Response
     */
    public function showPdf($convenio_id=0)
    {
        if(($convenio_id < 1))
            return abort(404, "Convênio não encontrado");
        
        $file_path = "convenio/$convenio_id/pdfs/cadastro_convenio.pdf";
        
//        dd(storage_path("app\\$file_path"));
        
        if(Storage::disk('local')->exists("/$file_path"))
            return response()->file(storage_path("app/$file_path"));
        
        return abort(404, "Arquivo não encontrado");
    }
    
    /**
     * Remove the specified document from storage.
     *
     * @param  Convenios  $convenio
     * @return \Illuminate\Http\Response
     */
    public function destroyPdf(Request $request, Convenios $convenio)
    {
        if (Gate::denies('convenio.controlar', $convenio)) {
            return abort(403, "Você não tem acesso a esta função");
        }
        
        $this->validate($request, [
            'documento' => 'required',
        ]);
        
        $data = [$request->documento => 'false'];
        
        $convenio->update($data);
        
        $file_path = "convenio/$id/pdfs/$request->documento.pdf";
        Storage::disk('local')->delete("/$file_path");
        
        return redirect()->route('convenio.edit', $id)
                            ->with('success','Documento removido com sucesso')
                            ->withInput();
    }
}
